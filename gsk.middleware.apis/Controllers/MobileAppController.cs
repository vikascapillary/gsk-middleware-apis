﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;
using System.Runtime.Caching;

namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class MobileAppController : ApiController
    {
        #region Variables
        private Logger logger;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private ApiCall apiCall = ApiCall.GetInstance;
        private readonly string connectionString, purchaseUnderstandingQuery, invoiceImageHistoryQuery;
        private string iunid;
        private readonly string liveOrgTillId;
        private readonly string  customerTrasnactionGetApi;
        private readonly DataTable  pmaster;
        static object productMasterTable = new object();
        const string productMasterCache = "productMasterCache";
        #endregion


        public MobileAppController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            GetMappings.Mappings();
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
            connectionString = string.Format("{0}&e;", connectionString);
            purchaseUnderstandingQuery = ConfigurationManager.AppSettings["PurchaseUnderstandingQuery"];
            invoiceImageHistoryQuery = ConfigurationManager.AppSettings["InvoiceImageHistoryQuery"];
            iunid = ConfigurationManager.AppSettings["IunId"];
            liveOrgTillId = ConfigurationManager.AppSettings["LiveOrgTillId"];
            customerTrasnactionGetApi = ConfigurationManager.AppSettings["IntouchTransactionGetAPI"];
            #endregion
            pmaster = GetMappings.pmaster;
        }

        #region Mobile APP
        [Route("v1.1/image/add")]
        public HttpResponseMessage GskImageUpload(RootObjectImageInvoice root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string jn = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn.ToString());
            try
            {
                if (root != null)
                {
                    foreach (ImageInvoice trans in root.root.image_invoice)
                    {
                        if (string.IsNullOrEmpty(trans.image_id) || string.IsNullOrEmpty(trans.retailar_code)
                            || string.IsNullOrEmpty(trans.stockiest_code) || string.IsNullOrEmpty(trans.image_status))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "All requests have failed due to errors";
                        }
                        else
                        {
                            bool flag = true;
                            try
                            {
                                DateTime d1 = Convert.ToDateTime(trans.image_upload_time);
                            }
                            catch (Exception)
                            {
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "All requests have failed due to errors";
                            }

                            if (trans.image_status.ToLower() != "open")
                            {
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "All requests have failed due to errors";
                            }
                            if (flag)
                            {
                                if (ImageAssigningToExectutive.executive.Count <= 0)
                                {
                                    ImageAssigningToExectutive exectutive = ImageAssigningToExectutive.GetInstance;
                                }
                            Start:
                                Queue<string> exec = ImageAssigningToExectutive.executive;
                                string name = exec.Dequeue();
                                logger.Debug("executive name: " + name);
                                if (string.IsNullOrEmpty(name))
                                {
                                    ImageAssigningToExectutive.executive.Clear();
                                    goto Start;
                                }
                                using (MySqlConnection connection = new MySqlConnection(connectionString))
                                {
                                    connection.Open();
                                    MySqlCommand mycommand = new MySqlCommand("sp_image_invoice", connection)
                                    {
                                        CommandType = CommandType.StoredProcedure
                                    };
                                    mycommand.Parameters.AddWithValue("@RetailerCode", trans.retailar_code != null ? trans.retailar_code : string.Empty);
                                    mycommand.Parameters.AddWithValue("@Stockiest_Code", trans.stockiest_code != null ? trans.stockiest_code : string.Empty);
                                    mycommand.Parameters.AddWithValue("@dealer_name", trans.dealer_name != null ? trans.dealer_name : string.Empty);
                                    mycommand.Parameters.AddWithValue("@drug_license", trans.drug_license != null ? trans.drug_license : string.Empty);
                                    mycommand.Parameters.AddWithValue("@gst", trans.gst != null ? trans.gst : string.Empty);
                                    mycommand.Parameters.AddWithValue("@address", trans.address != null ? trans.address : string.Empty);
                                    mycommand.Parameters.AddWithValue("@city", trans.city != null ? trans.city : string.Empty);
                                    mycommand.Parameters.AddWithValue("@state", trans.state != null ? trans.state : string.Empty);
                                    mycommand.Parameters.AddWithValue("@pin", trans.pin != null ? trans.pin : string.Empty);
                                    mycommand.Parameters.AddWithValue("@Image_id", trans.image_id != null ? trans.image_id : string.Empty);
                                    mycommand.Parameters.AddWithValue("@Image_Status", trans.image_status != null ? trans.image_status : string.Empty);
                                    mycommand.Parameters.AddWithValue("@storeId", trans.storeId != null ? trans.storeId : string.Empty);
                                    mycommand.Parameters.AddWithValue("@storeName", trans.storeName != null ? trans.storeName : string.Empty);
                                    mycommand.Parameters.AddWithValue("@storeDescription", trans.storeDescription != null ? trans.storeDescription : string.Empty);
                                    mycommand.Parameters.AddWithValue("@Image_Asigned_To", name != null ? name : string.Empty);
                                    mycommand.Parameters.AddWithValue("@image_upload_time", trans.image_upload_time != null ? trans.image_upload_time : Validation.IST().ToString("yyyy-MM-dd HH:mm:ss"));

                                    try
                                    {

                                        mycommand.ExecuteNonQuery();
                                        status.code = 200;
                                        status.success = true;
                                        status.message = "invoice image information saved successfully";
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex.ToString());

                                        status.code = 500;
                                        status.success = false;
                                        status.message = "All requests have failed due to errors";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    status.code = 500;
                    status.success = false;
                    status.message = "All requests have failed due to errors";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.success = false;
                status.message = "All requests have failed due to errors";
            }
            response.status = status;
            rootResponse.response = response;
            string jn1 = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
            logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/gsk-invoice-image-history/get")]
        [CacheOutput(ClientTimeSpan = 30, ServerTimeSpan = 30)]
        public HttpResponseMessage GetInvoiceImageHistory(string retailer_code = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            RootObjectImageInvoice ri = new RootObjectImageInvoice();
            RootImageInvoice roi = new RootImageInvoice();
            Status status = new Status();
            List<ImageInvoice> clslist = new List<ImageInvoice>();
            #region Old Code
            DataTable dt = gi.GetDataMapping(string.Format(@"{0} where RetailerCode like '{1}' order by image_upload_time", invoiceImageHistoryQuery, retailer_code), connectionString);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string image_status = dt.Rows[i]["Image_Status"].ToString().Trim();
                    if (image_status.ToLower().StartsWith("accepted"))
                    {
                        string[] splitStatus = image_status.Split(',');
                        image_status = splitStatus[0];
                    }
                    ImageInvoice cls = new ImageInvoice
                    {
                        id = dt.Rows[i]["transaction_number"].ToString(),
                        image_id = dt.Rows[i]["Image_id"].ToString(),
                        image_status = image_status,
                        image_update_time = dt.Rows[i]["status_update_time"].ToString()
                    };
                    DateTime dat = new DateTime();
                    try
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[i]["image_upload_time"].ToString()))
                        {
                            dat = Convert.ToDateTime(dt.Rows[i]["image_upload_time"].ToString());
                            cls.image_upload_time = dat.ToString(@"dd-MM-yyyy");
                        }
                        else
                        {
                            cls.image_upload_time = DateTime.Now.ToString(@"dd-MM-yyyy");
                        }
                    }
                    catch (Exception)
                    {
                        cls.image_upload_time = DateTime.Now.ToString(@"dd-MM-yyyy");
                    }

                    cls.stockiest_code = dt.Rows[i]["Stockiest_Code"].ToString();
                    cls.retailar_code = dt.Rows[i]["RetailerCode"].ToString();
                    clslist.Add(cls);
                }
                roi.image_invoice = clslist;
                ri.root = roi;
                ri.count = dt.Rows.Count;
                roi.status = status;
                status.code = 200;
                status.success = true;
                status.message = "success";
            }
            else
            {
                ri.root = roi;
                roi.status = status;
                status.code = 500;
                status.success = false;
                status.message = "failure";
            }
            #endregion

            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/gsk-purchase-understanding/get")]
        [CacheOutput(ClientTimeSpan = 4000, ServerTimeSpan = 4000)]
        public HttpResponseMessage GetGskPurchaseUnderstanding(string pharmacy_code = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            string query = string.Empty;
            if (!string.IsNullOrEmpty(pharmacy_code))
            {
                query = string.Format(@"{0} '{1}'", purchaseUnderstandingQuery, pharmacy_code);
            }
            else
            {
                query = purchaseUnderstandingQuery;
            }
            DataTable dt = gi.GetDataMapping(query, connectionString);
            RootPurchaseUnderstanding ri = new RootPurchaseUnderstanding();
            Status status = new Status();
            RootObjectPurchaseUnderstanding roi = new RootObjectPurchaseUnderstanding();
            List<PurchaseUnderstanding> clslist = new List<PurchaseUnderstanding>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PurchaseUnderstanding cls = new PurchaseUnderstanding
                    {
                        id = dt.Rows[i]["Serial_No"].ToString(),
                        product_id = dt.Rows[i]["Product_id"].ToString(),
                        sku_name = dt.Rows[i]["sku_name"].ToString(),
                        pharmacy_code = dt.Rows[i]["pharmacy_code"].ToString(),
                        pharmacy_name = dt.Rows[i]["pharmacy_name"].ToString(),
                        brick_name = dt.Rows[i]["brick_name"].ToString(),
                        brand_name = dt.Rows[i]["brand_name"].ToString(),
                        rep_code = dt.Rows[i]["rep_code"].ToString(),
                        rep_name = dt.Rows[i]["rep_name"].ToString(),
                        offer_plan = dt.Rows[i]["offer_plan"].ToString(),
                        offer_status = dt.Rows[i]["offer_status"].ToString(),
                        accepted_plan = dt.Rows[i]["accepted_plan"].ToString(),
                        frequency = dt.Rows[i]["frequency"].ToString(),
                        offer_status_date = dt.Rows[i]["offer_status_date"].ToString(),
                        offer_expiry_date = dt.Rows[i]["offer_expiry_date"].ToString()
                    };
                    try
                    {
                        cls.offer_expiry_date = Convert.ToDateTime(cls.offer_expiry_date).ToString("yyyy-MM-dd");
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                    }
                    //cls.offer_expiry_date = "2019-06-15";
                    cls.current_dt = dt.Rows[i]["current_dt"].ToString();
                    cls.reason_for_rejection = dt.Rows[i]["reason_for_rejection"].ToString();
                    cls.opportunity_value = dt.Rows[i]["opportunity_value"].ToString();
                    cls.commited_value = dt.Rows[i]["commited_value"].ToString();
                    cls.total_cost = dt.Rows[i]["total_cost"].ToString();
                    cls.total_margin = dt.Rows[i]["total_margin"].ToString();
                    cls.total_margin_baseline = dt.Rows[i]["total_margin_baseline"].ToString();
                    cls.total_discount = dt.Rows[i]["total_discount"].ToString();
                    cls.total_discount_baseline = dt.Rows[i]["total_discount_baseline"].ToString();
                    cls.secured_roi = dt.Rows[i]["secured_roi"].ToString();
                    cls.commited_volume = dt.Rows[i]["commited_volume"].ToString();
                    cls.commited_discount = dt.Rows[i]["committed_discount"].ToString();
                    cls.months_committed = dt.Rows[i]["months_committed"].ToString();
                    cls.days_commited = dt.Rows[i]["days_commited"].ToString();
                    cls.days_done = dt.Rows[i]["days_done"].ToString();
                    cls.days_remaining = dt.Rows[i]["days_remaining"].ToString();
                    cls.target_achieved = dt.Rows[i]["target_achieved"].ToString();
                    cls.last_refreshed = dt.Rows[i]["last_refreshed"].ToString();

                    if (!string.IsNullOrEmpty(cls.commited_discount))
                    {
                        try
                        {
                            double d = Convert.ToDouble(cls.commited_discount);
                            cls.commited_discount = string.Format(@"{0} %", d.ToString());
                        }
                        catch (Exception)
                        {
                        }
                    }

                    if (!string.IsNullOrEmpty(cls.commited_volume))
                    {
                        try
                        {
                            double d2 = Convert.ToDouble(cls.commited_volume);
                            cls.commited_volume = d2.ToString();
                        }
                        catch (Exception)
                        {
                        }
                    }

                    if (!string.IsNullOrEmpty(cls.last_refreshed))
                    {
                        try
                        {
                            DateTime date = Convert.ToDateTime(cls.last_refreshed);
                            cls.last_refreshed = date.ToString("yyyy-MMM-dd");
                        }
                        catch (Exception)
                        {
                            cls.last_refreshed = string.Empty;
                        }
                    }

                    cls.opportunity_value = cls.commited_volume;
                    cls.total_discount = cls.commited_discount;

                    clslist.Add(cls);
                }
                ri.purchase_understandings = clslist;
                roi.root = ri;
                ri.status = status;
                status.code = 200;
                status.success = true;
                status.message = "success";
            }
            else
            {
                roi.root = ri;
                ri.status = status;
                status.code = 500;
                status.success = false;
                status.message = "failure";
            }
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            //logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [HttpGet]
        [Route("v1.1/gsk/customer/transactions")]
        [CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage TransactionGet(string mobile = "")
        {
            TransRootObject tro = new TransRootObject();
            TransCustomer customer = new TransCustomer();
            TransResp tr = new TransResp();
            TransStatus ts = new TransStatus();
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            TransTransactions tlist = new TransTransactions();
            TransTransaction tt = new TransTransaction();
            List<TransTransaction> ttlist = new List<TransTransaction>();
            string otherOrgGetApi = string.Empty;
            string liveOrgGetApi = string.Format(@"{0}&mobile={1}&limit=200", customerTrasnactionGetApi, mobile);
            logger.Debug(liveOrgGetApi);
            string liveOrgTransGetResponse = apiCall.IntouchMakeRequest(liveOrgGetApi, "", liveOrgTillId, "gsk123", "", "");
            logger.Debug(liveOrgTransGetResponse);

            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(TransRootObject));
            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(liveOrgTransGetResponse)))
            {
                TransRootObject response = (TransRootObject)deserializer.ReadObject(ms);
                customer = response.response.customer;
                ts = response.response.status;
                TransTransactions trans = response.response.customer.transactions;
                int counter = 0;
                foreach (TransTransaction t in trans.transaction)
                {
                    foreach(var f in t.custom_fields.field)
                    {
                        if (f.name=="image_id" && t.store != "Rejected")
                        {
                            counter++;
                            ttlist.Add(t);
                            logger.Debug(string.Format(@"number{0} is having {1} image id", t.number, f.value));
                        }
                    }
                }
                logger.Debug(counter.ToString());
            }
            
            foreach (TransTransaction trans in ttlist)
            {
                if (trans.type.ToUpper() == "RETURN" && trans.amount != "0")
                {
                    trans.amount = string.Format("-{0}", trans.amount);
                }
                try
                {
                    TransCustomFields cf = trans.custom_fields;
                    foreach (Field c in cf.field)
                    {
                        if (c.name == "dealer_name")
                        {
                            if (!string.IsNullOrEmpty(c.value) && trans.store == "admin")
                            {
                                trans.store = c.value;
                            }
                        }
                    }

                    foreach (LineItem line in trans.line_items.line_item)
                    {
                        if (line.type.ToUpper() == "RETURN")
                        {
                            line.qty = string.Format("-{0}", line.qty);
                            line.amount = string.Format("-{0}", line.amount);
                            line.rate = string.Format("-{0}", line.rate);
                            line.value = string.Format("-{0}", line.value);
                            var desc = from i in pmaster.AsEnumerable()
                                       where i.Field<string>("item_code") == line.item_code
                                       select new
                                       {
                                           item_desc = i.Field<string>("item_description")
                                       };
                            foreach (var d in desc)
                            {
                                line.description = d.item_desc;
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
            tr.customer = customer;
            tr.status = ts;
            tlist.transaction = ttlist;
            customer.transactions = tlist;
            tro.response = tr;
            string jn1 = JsonConvert.SerializeObject(tro, Formatting.Indented);
            // logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, tro);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        #endregion
    }
}