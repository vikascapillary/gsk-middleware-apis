﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;
using System.Runtime.Caching;


namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class StockiestsController : ApiController
    {
        #region Variables
        private Logger logger;
        private readonly string connectionString;
        #endregion

        public StockiestsController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
           connectionString = string.Format("{0}&e;", connectionString);
            #endregion
        }

        #region Stockiest APIs

        [Route("v1.1/transaction/add")]
        public HttpResponseMessage TransactionAdd(RootObject root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);

            #region Local Variables
            StringBuilder sb = new StringBuilder();
            Status status = new Status();
            TransactionResponse tresponse = new TransactionResponse();
            ItemStatus itemStatus = new ItemStatus();
            LineItem lineitem = new LineItem();
            Transactions transRes = new Transactions();
            RootObjectAddResponse rootRes = new RootObjectAddResponse();
            Response res = new Response();
            List<TransactionResponse> listTresponse = new List<TransactionResponse>();
            string filename = string.Empty;
            string stockistcode = string.Empty;
            #endregion
            try
            {
                if (root != null)
                {
                    string json2 = JsonConvert.SerializeObject(root, Formatting.Indented);

                    #region Reading Request
                    foreach (Transaction trans in root.root.transaction)
                    {
                        string custDl = trans.customer.retailer_dl;
                        string custGst = trans.customer.retailer_gst;
                        string requestType = trans.type;
                        tresponse.billing_time = trans.billing_time;
                        string number = trans.number;

                        #region Transaction Date Validation
                        bool checkDate = DateTime.TryParse(trans.billing_time, out DateTime date);
                        if (!checkDate)
                        {
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Transaction Date is not correct";
                            itemStatus.code = 400;
                        }
                        #endregion

                        string billingtime = Convert.ToDateTime(date).ToString("yyyymmddHHmmss");
                        stockistcode = trans.stockiest_code;
                        filename = string.Format("{0}_{1}_{2}_{3}{4}", number, stockistcode, billingtime, DateTime.Now.ToString("yyyyMMddHHmmss"), ".json");

                        #region Retailer Identifier Validation
                        if (string.IsNullOrEmpty(custDl) && string.IsNullOrEmpty(custGst))
                        {
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Retailer Identifer is not correct";
                            itemStatus.code = 400;
                        }
                        #endregion

                        #region Request Type Validation
                        else if (requestType != "regular" && requestType.ToLower() != "return")
                        {
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Transaction Type is not correct";
                            itemStatus.code = 400;
                        }
                        #endregion

                        #region Transaction Number Validation
                        else if (string.IsNullOrEmpty(number))
                        {
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Transaction number is not correct";
                            itemStatus.code = 400;
                        }
                        #endregion

                        else
                        {
                            tresponse.number = number;
                            tresponse.delivery_status = "Saved";
                            tresponse.type = trans.type;
                            tresponse.stockiestCode = trans.stockiest_code;
                            tresponse.record_id = filename;
                            LineItems lineItems = trans.line_items;

                            #region Transaction Value Validation
                            double transAmount = Validation.AmountCheck(trans.amount);
                            if (transAmount < 0)
                            {
                                status.success = false;
                                status.message = "Request is not in Proper format";
                                status.code = 500;

                                itemStatus.success = false;
                                itemStatus.message = "Transaction Amount is not correct";
                                itemStatus.code = 400;
                            }
                            if (!string.IsNullOrEmpty(trans.gross_amount))
                            {
                                double transGross = Validation.AmountCheck(trans.gross_amount);
                                if (transGross < 0)
                                {
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "Transaction Gross Amount is not correct";
                                    itemStatus.code = 400;

                                }
                            }
                            double transDiscount = Validation.AmountCheck(trans.discount);
                            if (transDiscount < 0)
                            {
                                status.success = false;
                                status.message = "Request is not in Proper format";
                                status.code = 500;

                                itemStatus.success = false;
                                itemStatus.message = "Transaction Discount is not correct";
                                itemStatus.code = 400;

                            }
                            #endregion

                            #region Line Item Value Validation
                            foreach (LineItem line in lineItems.line_item)
                            {
                                double rate = Validation.AmountCheck(line.rate);
                                double value = Validation.AmountCheck(line.value);
                                double amount = Validation.AmountCheck(line.amount);
                                double discount = Validation.AmountCheck(line.discount);
                                double qty = Validation.IntCheck(line.qty);

                                if (rate < 0)
                                {
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item Rate is not correct";
                                    itemStatus.code = 400;
                                }
                                if (value < 0)
                                {
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item Value is not correct";
                                    itemStatus.code = 400;

                                }
                                if (amount < 0)
                                {
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item amount is not correct";
                                    itemStatus.code = 400;
                                }
                                if (discount < 0)
                                {
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item discount is not correct";
                                    itemStatus.code = 400;

                                }
                                if (qty < 0)
                                {
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item qty is not correct";
                                    itemStatus.code = 400;

                                }
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region Creating Request Json 
                    string jsonfilepath = @"C:/GSK/RequestFiles/";

                    if (!Directory.Exists(jsonfilepath))
                    {
                        Directory.CreateDirectory(jsonfilepath);
                    }

                    if (Directory.Exists(jsonfilepath))
                    {
                        {
                            StreamWriter sw = new StreamWriter(string.Format(@"{1}\{0}", filename, jsonfilepath));
                            sw.WriteLine(json2.ToString());
                            sw.Close();
                            sw.Dispose();
                        }
                    }
                    #endregion

                    #region Success Response
                    if (status.code != 500)
                    {
                        status.success = true;
                        status.message = "Request is in Proper format";
                        status.code = 200;

                        itemStatus.success = true;
                        itemStatus.message = "Trasnaction saved in AWS server";
                        itemStatus.code = 600;
                    }
                    res.status = status;
                    tresponse.item_status = itemStatus;
                    listTresponse.Add(tresponse);
                    transRes.transaction = listTresponse;
                    res.status = status;
                    res.transactions = transRes;
                    rootRes.response = res;
                    #endregion

                    sb.Append(string.Format("Request from {1} {2}{0}{2}", json2, stockistcode, Environment.NewLine));

                }
                else
                {
                    #region Failure Response
                    status.success = false;
                    status.message = "Request in bad format";
                    status.code = 500;

                    itemStatus.success = false;
                    itemStatus.message = "Request can't be saved to AWS server";
                    itemStatus.code = 400;
                    res.status = status;
                    tresponse.item_status = itemStatus;
                    listTresponse.Add(tresponse);
                    transRes.transaction = listTresponse;
                    res.status = status;
                    res.transactions = transRes;
                    rootRes.response = res;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                #region Failure Response
                status.success = false;
                status.message = "Request in bad format";
                status.code = 500;

                itemStatus.success = false;
                itemStatus.message = "Request can't be saved to AWS server";
                itemStatus.code = 400;
                res.status = status;
                tresponse.item_status = itemStatus;
                listTresponse.Add(tresponse);
                transRes.transaction = listTresponse;
                res.status = status;
                res.transactions = transRes;
                rootRes.response = res;
                #endregion
            }
            if (rootRes != null)
            {
                string responseToConsumer = JsonConvert.SerializeObject(rootRes, Formatting.Indented);
                sb.Append(string.Format("Request from {1} {2}{0}{2}", responseToConsumer, stockistcode, Environment.NewLine));
            }
            logger.Info(sb.ToString());
            string jn = JsonConvert.SerializeObject(rootRes, Formatting.Indented);
            logger.Debug(jn.ToString());
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootRes);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/sku-mapping-failre/add")]
        public HttpResponseMessage SkuMappingFailre(RootObjectSkuMappingFailure root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string jn1 = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn1.ToString());
            try
            {
                if (root != null)
                {
                    foreach (SkuMappingFailure trans in root.root.sku_mapping_failure)
                    {
                        if (string.IsNullOrEmpty(trans.stockiest_sku_desc))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "All requests have failed due to errors";
                        }
                        else
                        {
                            using (MySqlConnection connection = new MySqlConnection(connectionString))
                            {
                                connection.Open();
                                MySqlCommand mycommand = new MySqlCommand("sp_sku_failure", connection)
                                {
                                    CommandType = CommandType.StoredProcedure
                                };
                                mycommand.Parameters.AddWithValue("@Stockiest_SKU_Name", trans.stockiest_sku_desc.ToString());
                                mycommand.Parameters.AddWithValue("@Invoice_No", trans.invoice_number.ToString());
                                mycommand.Parameters.AddWithValue("@Invoice_Date", trans.invoice_date.ToString());
                                mycommand.Parameters.AddWithValue("@Failure_Date", trans.failure_date.ToString());
                                mycommand.Parameters.AddWithValue("@Stockiest_Code", trans.stockiest_code.ToString());
                                mycommand.Parameters.AddWithValue("@Mapping_Date", "2019-01-01 00:00:00");
                                mycommand.Parameters.AddWithValue("@RetailerCode", "");
                                mycommand.Parameters.AddWithValue("@qty", "0");
                                mycommand.Parameters.AddWithValue("@Image_id", "");
                                mycommand.Parameters.AddWithValue("@Mapping_Flag", "0");
                                try
                                {

                                    mycommand.ExecuteNonQuery();
                                    status.code = 200;
                                    status.success = true;
                                    status.message = "Unmatched Sku data saved successfully";
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex.ToString());

                                    status.code = 500;
                                    status.success = false;
                                    status.message = "All requests have failed due to errors";
                                }
                            }
                        }
                    }
                }
                else
                {
                    status.code = 500;
                    status.success = false;
                    status.message = "All requests have failed due to errors";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.success = false;
                status.message = "All requests have failed due to errors";
            }
            response.status = status;
            rootResponse.response = response;
            string jn = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
            logger.Debug(jn);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        #endregion
    }

}