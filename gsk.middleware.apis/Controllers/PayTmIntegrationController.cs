﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;
using System.Runtime.Caching;


namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class PayTmIntegrationController : ApiController
    {
        #region Variables
        private Logger logger;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private ApiCall apiCall = ApiCall.GetInstance;
        private readonly string connectionString;
        private readonly string merchantguid, aesKey, saleswalletid, mId;
        private readonly string payTmAmountAddApi, payTmTxnCheckApi, retryCount, sleepTime;
        private string payTmFailureCode;
        private string txnFailureCode;
        private readonly string  liveOrgTillId;
        private readonly string pointRedeemApi;
        private readonly string pointsReddem, pointsIsRedeem;
        #endregion

        public PayTmIntegrationController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
          connectionString = string.Format("{0}&e;", connectionString);
            merchantguid = ConfigurationManager.AppSettings["Merchantguid"];
            aesKey = ConfigurationManager.AppSettings["AesKey"];
            saleswalletid = ConfigurationManager.AppSettings["Saleswalletid"];
            mId = ConfigurationManager.AppSettings["MId"];
            payTmAmountAddApi = ConfigurationManager.AppSettings["PayTmAddAmountApi"];
            payTmTxnCheckApi = ConfigurationManager.AppSettings["PayTmCheckTxnApi"];
            retryCount = ConfigurationManager.AppSettings["RetryCount"];
            sleepTime = ConfigurationManager.AppSettings["SleepTime"];
            payTmFailureCode = ConfigurationManager.AppSettings["PayTMFailureCode"];
            txnFailureCode = ConfigurationManager.AppSettings["TxnFailureCodes"];
            liveOrgTillId = ConfigurationManager.AppSettings["LiveOrgTillId"];
            pointRedeemApi = ConfigurationManager.AppSettings["IntouchPointRedeemAPI"];
            pointsReddem = ConfigurationManager.AppSettings["PointsRedeem"];
            pointsIsRedeem = ConfigurationManager.AppSettings["IntouchPointIsRedeemAPI"];
            #endregion
        }

        #region PayTM Integration
        [Route("v1.1/payTM/add")]
        public HttpResponseMessage PayTm(RootObjectPayTmFromApp root)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(@"DELETE FROM paytm_process WHERE `datetime` < (NOW() - INTERVAL 2 MINUTE)", connection);
                    cmd.ExecuteNonQuery();
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
            }

            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            StatusResponseRootObejct ro = new StatusResponseRootObejct();
            string jn1 = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn1);
            string mobile = string.Empty;
            bool flag = true;
            if (root != null)
            {
                string payTmMobile = root.request.pay_tm_cap_mobile;
                string points = root.points;
                string amount = root.request.amount;
                string isRedeemApi = string.Format(@"{0}&mobile={2}&points={1}&issue_otp=false&skip_validation=true", pointsIsRedeem, points, payTmMobile);
                string isRedeemResponse = apiCall.IntouchMakeRequest(isRedeemApi, "", liveOrgTillId, "gsk123", "", "");
                DataContractJsonSerializer desIsRedeem = new DataContractJsonSerializer(typeof(IsRedeemRootObject));
                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(isRedeemResponse)))
                {
                    IsRedeemRootObject resIsRedeem = (IsRedeemRootObject)desIsRedeem.ReadObject(ms);
                    int st = resIsRedeem.response.points.redeemable.item_status.code;
                    string isRedeem = resIsRedeem.response.points.redeemable.is_redeemable;
                    int isRedeemValue = resIsRedeem.response.points.redeemable.points_redeem_value;
                    try
                    {
                        if (st != 800 && isRedeem.ToLower() != "true")
                        {
                            flag = false;
                            status.code = 500;
                            status.success = false;
                            status.message = "All requests have failed due to errors";
                        }
                        else if (amount != isRedeemValue.ToString())
                        {
                            flag = false;
                            status.code = 500;
                            status.success = false;
                            status.message = "Point value not matching with payTm amount";
                        }
                    }
                    catch (Exception)
                    {
                        flag = false;
                        status.code = 500;
                        status.success = false;
                        status.message = "All requests have failed due to errors";
                    }
                }

                mobile = payTmMobile.Substring(2, 10);
                string ipAddess = root.ipAddress;
                string metaData = root.metadata;
                double amountCheck = Validation.AmountCheck(amount);
                try
                {
                    DataTable dt = gi.GetDataMapping(string.Format(@"select * from paytm_process where mobile='{0}'", mobile), connectionString);
                    if (dt.Rows.Count > 0)
                    {
                        flag = false;
                        status.code = 500;
                        status.success = false;
                        status.message = "Previos PayTM request is already in process";
                        logger.Debug(string.Format("Previos PayTM request is already in process for mobile {0}", mobile));
                    }
                    else
                    {
                        using (MySqlConnection connection = new MySqlConnection(connectionString))
                        {
                            connection.Open();
                            MySqlCommand mycommand = new MySqlCommand("sp_paytm_procces", connection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            mycommand.Parameters.AddWithValue("@mobile", mobile);
                            try
                            {

                                mycommand.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.ToString());
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "Previos PayTM request is already in process";
                                logger.Debug(string.Format("Previos PayTM request is already in process for mobile {0}", mobile));
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    logger.Error(ex.ToString());
                }
                if (flag)
                { 
                    if (amountCheck > 0)
                    {
                        string verify_request = string.Empty;
                        string verify_response = string.Empty;
                        string verify_message = string.Empty;
                        string request_two = string.Empty;
                        string response_two = string.Empty;
                        string message_two = string.Empty;
                        logger.Debug("merchantguid " + merchantguid);
                        logger.Debug("aesKey " + aesKey);
                        logger.Debug("saleswalletid" + saleswalletid);
                        PayTmRequest request = new PayTmRequest();
                        RootObejectPayTM ri = new RootObejectPayTM();
                        request.amount = amount;
                        request.requestType = "VERIFY";
                    Start:
                        string orderid = DateTime.Now.Ticks.ToString();
                        logger.Debug("orderid " + orderid);
                        request.appliedToNewUsers = "N";
                        request.callbackURL = @"https://paytm.com/market/salesToUserCredit";
                        request.currencyCode = "INR";
                        request.merchantGuid = merchantguid;
                        request.merchantOrderId = orderid;
                        request.payeeEmailId = "";
                        request.payeePhoneNumber = mobile;
                        request.payeeSsoId = "";
                        request.salesWalletGuid = saleswalletid;
                        request.salesWalletName = "";
                        request.pendingDaysLimit = "0";
                        ri.request = request;
                        ri.ipAddress = ipAddess;
                        ri.metadata = metaData;
                        ri.operationType = "SALES_TO_USER_CREDIT";
                        ri.platformName = "PayTM";
                        string json2 = JsonConvert.SerializeObject(ri, Formatting.None);
                        string checksum = paytm.CheckSum.generateCheckSumByJson(aesKey, json2);
                        string payTmResponse = apiCall.PayTm(json2, payTmAmountAddApi, checksum, merchantguid);
                        if (!string.IsNullOrEmpty(payTmResponse))
                        {
                            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(PayTmResponse));
                            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(payTmResponse)))
                            {
                                PayTmResponse roi = (PayTmResponse)deserializer.ReadObject(ms);
                                string message = roi.statusMessage;
                                if (message == @"ACCEPTED" && request.requestType == "VERIFY")
                                {
                                    request.requestType = null;
                                    verify_message = message;
                                    verify_request = json2;
                                    verify_response = payTmResponse;
                                    goto Start;
                                }
                                else if (message == @"ACCEPTED" && request.requestType == null)
                                {
                                    status.code = 200;
                                    status.success = true;
                                    status.message = "success";

                                    message_two = message;
                                    request_two = json2;
                                    response_two = payTmResponse;
                                    try
                                    {
                                        PayTmResponse payTmCheck = new PayTmResponse();
                                        ro = PayTmOrderIdStatusCheck(orderid, out payTmCheck);
                                        string[] payTmErrorCodesArray = payTmFailureCode.Split(',');
                                        string[] txnErrorCodesArray = txnFailureCode.Split(',');
                                        if (ro.paytm_trans_status_failure != null)
                                        {
                                            if (ro.paytm_trans_status_failure.statusCode == "GE_1044")
                                            {
                                                status.code = 200;
                                                status.success = true;
                                                status.message = ro.paytm_trans_status_failure.statusMessage;
                                            }
                                            else if (payTmErrorCodesArray.Contains(ro.paytm_trans_status_failure.statusCode))
                                            {
                                                status.code = 500;
                                                status.success = false;
                                                status.message = ro.paytm_trans_status_failure.statusMessage;
                                            }
                                        }

                                        if (ro.txnList != null)
                                        {
                                            foreach (TxnList d in ro.txnList)
                                            {
                                                if (payTmErrorCodesArray.Contains(d.txnErrorCode))
                                                {
                                                    status.code = 500;
                                                    status.success = false;
                                                    status.message = d.message;
                                                }
                                                if (d.message.ToLower() == "success")
                                                {
                                                    status.code = 200;
                                                    status.success = true;
                                                    status.message = d.message;
                                                }
                                            }
                                        }

                                        ro.paytm_amount_add_response = roi;
                                        string m = string.Empty;
                                        string txn_response = string.Empty;
                                        if (ro.txnList != null)
                                        {
                                            foreach (TxnList d in ro.txnList)
                                            {
                                                txn_response = JsonConvert.SerializeObject(d, Formatting.None);
                                                m = d.message;
                                            }
                                        }
                                        else
                                        {
                                            txn_response = JsonConvert.SerializeObject(payTmCheck, Formatting.None);
                                            m = payTmCheck.statusMessage;
                                        }
                                        string rq = string.Empty;
                                        string re = string.Empty;
                                        if (status.code == 200 && pointsReddem == "1")
                                        {
                                            string redeem = PointRedeem(mobile, points, out rq);
                                            re = redeem;
                                        }
                                        string[] arr = { mobile, orderid, verify_request, verify_response, verify_message, request_two, response_two, message_two, Validation.IST().ToString("yyyy-MM-dd HH:mm:ss"), points, txn_response, m, re, rq };
                                        LogPayTmCall(arr);
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex.ToString());
                                    }
                                }
                                else
                                {
                                    status.code = 500;
                                    status.success = false;
                                    status.message = "PAYTM Request failed";
                                }

                            }
                        }
                        else
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "PAYTM Request failed";
                        }
                    }
                    else
                    {
                        status.code = 500;
                        status.success = false;
                        status.message = "Amount value in not correct";
                    }
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        connection.Open();
                        MySqlCommand cmd = new MySqlCommand(string.Format(@"delete from paytm_process where mobile='{0}'", mobile), connection);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            response.status = status;
            rootResponse.response = response;
            ro.status = status;
            string jsn = JsonConvert.SerializeObject(ro, Formatting.Indented);
            logger.Debug("PayTM Response " + jsn);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ro);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        public StatusResponseRootObejct PayTmOrderIdStatusCheck(string orderId, out PayTmResponse payTmCheck)
        {
            int count = 0;
        START:
            payTmCheck = new PayTmResponse();
            System.Threading.Thread.Sleep(1000 * Convert.ToInt32(sleepTime));
            Status status = new Status();
            PayTMStatusCheck payTMStatusCheck = new PayTMStatusCheck();
            StatusResponseRootObejct roi1 = new StatusResponseRootObejct();
            RootObjectPayTmStatusCheck rootObjectPayTmStatusCheck = new RootObjectPayTmStatusCheck();
            try
            {
                payTMStatusCheck.requestType = "merchantTxnId";
                payTMStatusCheck.txnId = orderId;
                payTMStatusCheck.txnType = "SALES_TO_USER_CREDIT";
                payTMStatusCheck.mId = mId;
                rootObjectPayTmStatusCheck.ipAddress = "PayTM";
                rootObjectPayTmStatusCheck.platformName = "PayTM";
                rootObjectPayTmStatusCheck.operationType = "CHECK_TXN_STATUS";
                rootObjectPayTmStatusCheck.request = payTMStatusCheck;
                string json2 = JsonConvert.SerializeObject(rootObjectPayTmStatusCheck, Formatting.None);
                string checksum = paytm.CheckSum.generateCheckSumByJson(aesKey, json2);
                string payTmResponse = apiCall.PayTm(json2, payTmTxnCheckApi, checksum, merchantguid);
                logger.Debug(@"checksum " + checksum);
                try
                {
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(StatusResponseRootObejct));
                    using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(payTmResponse)))
                    {
                        StatusResponseRootObejct roi = (StatusResponseRootObejct)deserializer.ReadObject(ms);
                        foreach (TxnList trans in roi.txnList)
                        {
                            int status1 = trans.status;
                            string message = trans.message;
                            string txnGuid = trans.txnGuid;
                            if (status1 == 1 && message == "SUCCESS" && !string.IsNullOrEmpty(txnGuid))
                            {
                                roi.txn_status_check = status;
                                roi.order_id = orderId;
                                status.code = 200;
                                status.success = true;
                                status.message = "success";
                            }
                            else
                            {
                                count++;
                                if (count < Convert.ToInt32(retryCount))
                                {
                                    goto START;
                                }
                                roi.txn_status_check = status;
                                roi.order_id = orderId;
                                status.code = 500;
                                status.success = false;
                                status.message = "failure";
                            }
                        }
                        return roi;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(PayTmResponse));
                    using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(payTmResponse)))
                    {
                        payTmCheck = (PayTmResponse)deserializer.ReadObject(ms);
                        roi1.paytm_trans_status_failure = payTmCheck;
                        if (roi1.paytm_trans_status_failure.statusCode == "GE_1044")
                        {
                            count++;
                            logger.Error(string.Format(@"Transaction no: {3}, Status Retry count {0}{2}{1}", count, ex.ToString(), Environment.NewLine, orderId));
                            if (count < Convert.ToInt32(retryCount))
                            {
                                goto START;
                            }
                        }
                        roi1.txn_status_check = status;
                        roi1.order_id = orderId;
                        status.code = 500;
                        status.success = false;
                        status.message = "failure";
                    }
                }
            }
            catch (Exception)
            {
                roi1.txn_status_check = status;
                roi1.order_id = orderId;
                status.code = 500;
                status.success = false;
                status.message = "failure";
            }
            string jn1 = JsonConvert.SerializeObject(roi1, Formatting.Indented);
            logger.Debug(jn1);
            return roi1;
        }
        public void LogPayTmCall(string[] arr)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                MySqlCommand mycommand = new MySqlCommand("sp_log_payTM", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };
                mycommand.Parameters.AddWithValue("@mobile_no", arr[0]);
                mycommand.Parameters.AddWithValue("@txn_id", arr[1]);
                mycommand.Parameters.AddWithValue("@verify_request", arr[2]);
                mycommand.Parameters.AddWithValue("@verify_response", arr[3]);
                mycommand.Parameters.AddWithValue("@verify_message", arr[4]);
                mycommand.Parameters.AddWithValue("@request", arr[5]);
                mycommand.Parameters.AddWithValue("@response", arr[6]);
                mycommand.Parameters.AddWithValue("@message", arr[7]);
                mycommand.Parameters.AddWithValue("@api_call_time", arr[8]);
                mycommand.Parameters.AddWithValue("@points", arr[9]);
                mycommand.Parameters.AddWithValue("@txn_status_response", arr[10]);
                mycommand.Parameters.AddWithValue("@txn_status_message", arr[11]);
                mycommand.Parameters.AddWithValue("@PointRedeemResponse", arr[12]);
                mycommand.Parameters.AddWithValue("@PointRedeemRequest", arr[13]);
                try
                {
                    connection.Open();
                    mycommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
        }
        public string PointRedeem(string mobile, string points, out string request)
        {
            int counter = 0;
        Start:
            RedeemedRootObject rro = new RedeemedRootObject();
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            PointsCustomer cust = new PointsCustomer();
            PointsRedeem pointRedeem = new PointsRedeem();
            List<PointsRedeem> lpr = new List<PointsRedeem>();
            PointsRoot pr = new PointsRoot();
            PointsRootObject pro = new PointsRootObject();
            cust.mobile = mobile;
            pointRedeem.points_redeemed = points;
            pointRedeem.redemption_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            pointRedeem.customer = cust;
            lpr.Add(pointRedeem);
            pr.redeem = lpr;
            pro.root = pr;
            string jn = JsonConvert.SerializeObject(pro, Formatting.Indented);
            logger.Debug(pro);
            request = jn;
            string api = string.Format(@"{0}&skip_validation=true", pointRedeemApi);
            string response = apiCall.IntouchMakeRequest(api, jn, liveOrgTillId, "gsk123", "", "");
            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RedeemedRootObject));
            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(response)))
            {
                rro = (RedeemedRootObject)deserializer.ReadObject(ms);
                int status = rro.response.status.code;
                if (status != 200)
                {
                    counter++;
                    if (counter < 3)
                    {
                        goto Start;
                    }
                }
            }
            return response;
        }

        [HttpGet]
        [Route("v1.1/payTM/order-id/get")]
        [CacheOutput(ClientTimeSpan = 1000, ServerTimeSpan = 1000)]
        public HttpResponseMessage PayTmGetOrderId(string mobile_no = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            List<OrderIds> orderIds = new List<OrderIds>();
            Status status = new Status();
            RootOrderIds roi = new RootOrderIds();
            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        DataTable dt = gi.GetDataMapping(string.Format(" where mobile_no='{0}'", mobile_no, ConfigurationManager.AppSettings["PayTm_Transaction"]), connectionString);

                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                OrderIds cls = new OrderIds
                                {
                                    order_id = dt.Rows[i]["txn_id"].ToString()
                                };
                                orderIds.Add(cls);
                                status.code = 200;
                                status.message = "success";
                                status.success = true;
                                roi.status = status;
                            }
                            roi.order_ids = orderIds;
                        }
                        else
                        {
                            status.code = 500;
                            status.message = "success";
                            status.success = false;
                            roi.status = status;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                        status.code = 500;
                        status.message = "failure";
                        status.success = false;
                        roi.status = status;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.message = "failure";
                status.success = false;
                roi.status = status;
            }
            string jn1 = JsonConvert.SerializeObject(roi, Formatting.Indented);
            //logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, roi);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [HttpGet]
        [Route("v1.1/payTM/txn-status/get")]
        [CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage PayTmStatusCheck(string txnId = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            PayTmResponse payTmCheck = new PayTmResponse();
            StatusResponseRootObejct roi1 = PayTmOrderIdStatusCheck(txnId, out payTmCheck);
            string jn1 = JsonConvert.SerializeObject(roi1, Formatting.Indented);
            // logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, roi1);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        #endregion
    }
}