﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;

namespace gsk.middleware.apis.Controllers
{

    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class HelpDeskController : ApiController
    {
        #region Variables
        private Logger logger;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private ApiCall apiCall = ApiCall.GetInstance;
        string getBillNumberApi, rejectedInvoiceToCrm;
        string rejectedTillAppned;
        private MultiPageInvoiceHandling multiPageInvoiceHandling = MultiPageInvoiceHandling.GetInstance;
        private readonly string connectionString;
        string invoiceImageQuery;
        private readonly string intouchTrasnactionAddApi;
        private string iunid;
        private readonly string iunPwd;
        private readonly string getS3ApiUrl;
        private readonly string auth;
        private readonly string retailerImageQuery;
        private readonly string appendingNumberTill, liveOrgTillId, customerGetApi;
        private readonly string brandName, customerTrasnactionGetApi;
        private readonly DataTable mfg, pmapping, pmaster, stockiestMapping;
        string getBillNumber;
        static object mfgTable = new object();
        const string mfgCache = "mfgCache";
        static object productMappingTable = new object();
        const string productMappingCache = "productMappingCache";
        static object productMasterTable = new object();
        const string productMasterCache = "productMasterCache";

        #endregion
        public HelpDeskController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            GetMappings.Mappings();
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
           connectionString = string.Format("{0}&e;", connectionString);
            invoiceImageQuery = ConfigurationManager.AppSettings["InvoiceImageQuery"];
            rejectedInvoiceToCrm = ConfigurationManager.AppSettings["RejectedInvoiceToCRM"];
            rejectedTillAppned = ConfigurationManager.AppSettings["AppendingNumberInRejectedTillId"];
            retailerImageQuery = ConfigurationManager.AppSettings["RetailerImageQuery"];
            getBillNumberApi = ConfigurationManager.AppSettings["IntouchBillNumberGetAPI"];
            intouchTrasnactionAddApi = ConfigurationManager.AppSettings["IntouchTransactionAddAPI"];
            getBillNumber = ConfigurationManager.AppSettings["GetBillNumber"];
            iunid = ConfigurationManager.AppSettings["IunId"];
            iunPwd = ConfigurationManager.AppSettings["IunPwd"];

            getS3ApiUrl = ConfigurationManager.AppSettings["GetS3ApiURL"];
            auth = ConfigurationManager.AppSettings["GetS3ApiUrlAuth"];
            brandName = ConfigurationManager.AppSettings["BrandName"];
            appendingNumberTill = ConfigurationManager.AppSettings["AppendingNumberInTillId"];
            liveOrgTillId = ConfigurationManager.AppSettings["LiveOrgTillId"];
            customerGetApi = ConfigurationManager.AppSettings["CustomerGetAPI"];
            customerTrasnactionGetApi = ConfigurationManager.AppSettings["IntouchTransactionGetAPI"];
            #endregion

            mfg = GetMappings.mfg;
            pmapping = GetMappings.pmapping;
            pmaster = GetMappings.pmaster;
            stockiestMapping = GetMappings.stockiestMapping;
        }
      

        #region HelpDesk APIs
        [Route("v1.1/retialer-uploaded-image/get")]
        [CacheOutput(ClientTimeSpan = 30, ServerTimeSpan = 30)]
        public HttpResponseMessage GetRetailerImageUploaded(string login_id = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            RootObjectRetailerUploadedImage ri = new RootObjectRetailerUploadedImage();
            RootRetailerUploadedImage roi = new RootRetailerUploadedImage();
            List<RetailerUploadedImage> clslist = new List<RetailerUploadedImage>();
            ApiIdentity fi = (ApiIdentity)System.Web.HttpContext.Current.User.Identity;
            string id = fi.User.UserName;
            int count = 0;
            #region Code
            DataTable dt = gi.GetDataMapping(string.Format(@"{0} and Image_Asigned_To='{1}' and `ocr_job_id` is not null order by image_upload_time", retailerImageQuery, id), connectionString);
            if (dt.Rows.Count > 0)
            {
                count = dt.Rows.Count;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RetailerUploadedImage cls = new RetailerUploadedImage
                    {
                        retailar_code = dt.Rows[i]["RetailerCode"].ToString()
                    };
                    clslist.Add(cls);
                }
                roi.image_invoice = clslist;
                ri.root = roi;
            }
            #endregion
            ri.count = clslist.Count;
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/gsk-invoice-image/get")]
        //[CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage GetInvoiceImage([FromUri]PagingParameterModel pagingparametermodel, string stockiest_code = "", string retailer_code = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            var url = Request.RequestUri;
            logger.Debug("request_Id " + request_Id);
            ApiIdentity fi = (ApiIdentity)System.Web.HttpContext.Current.User.Identity;
            string id = fi.User.UserName;
            string query = string.Empty;
            int TotalCount = 0;
            int PageSize = 0;
            int CurrentPage = 0;
            int TotalPages = 0;
            string previousPage = "";
            string nextPage = "";

            if (!string.IsNullOrEmpty(stockiest_code) && !string.IsNullOrEmpty(retailer_code))
            {
                query = string.Format(@"{0} and Stockiest_Code like '{1}' and 	RetailerCode like '{2}' and Image_Asigned_To='{3}' order by image_upload_time", invoiceImageQuery, stockiest_code, retailer_code, id);
            }
            else if (!string.IsNullOrEmpty(stockiest_code) && string.IsNullOrEmpty(retailer_code))
            {
                query = string.Format(@"{0} and Stockiest_Code like '{1}'  and Image_Asigned_To='{3}' order by image_upload_time", invoiceImageQuery, stockiest_code, retailer_code, id);
            }
            else if (string.IsNullOrEmpty(stockiest_code) && !string.IsNullOrEmpty(retailer_code))
            {
                query = string.Format(@"{0} and RetailerCode like '{2}'  and Image_Asigned_To='{3}' order by image_upload_time", invoiceImageQuery, stockiest_code, retailer_code, id);
            }
            else
            {
                query = string.Format("{0} and Image_Asigned_To='{1}'", invoiceImageQuery, id);
            }

            if (id == "ocr_user")
            {
                query = string.Format("SELECT * FROM `Invoice_Image` WHERE Image_Status like 'OPEN%' and ocr_job_id is null order by image_upload_time", invoiceImageQuery, id);
            }

            DataTable dt = gi.GetDataMapping(query, connectionString);
            int count = dt.Rows.Count;
            List<DataRow> items = dt.AsEnumerable().ToList();
            if (url.ToString().Contains("pageNumber"))
            {
                CurrentPage = pagingparametermodel.pageNumber;
                PageSize = pagingparametermodel.pageSize;
                TotalCount = count;
                TotalPages = (int)Math.Ceiling(count / (double)PageSize);
                count = TotalPages;
                items = items.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();
                previousPage = CurrentPage > 1 ? "Yes" : "No";
                nextPage = CurrentPage < TotalPages ? "Yes" : "No";
            }
            RootObjectImageInvoice ri = new RootObjectImageInvoice();
            Status status = new Status();
            RootImageInvoice roi = new RootImageInvoice();
            List<ImageInvoice> clslist = new List<ImageInvoice>();
            if (items.Count > 0)
            {
                ri.page_count = items.Count;
                for (int i = 0; i < items.Count; i++)
                {
                    if (string.IsNullOrEmpty(items[i]["ocr_job_id"].ToString()) || id != "ocr_user")
                    {
                        ImageInvoice cls = new ImageInvoice
                        {
                            id = items[i]["SerialNo"].ToString(),
                            image_id = items[i]["Image_id"].ToString(),
                            image_status = items[i]["Image_Status"].ToString(),
                            image_update_time = items[i]["status_update_time"].ToString(),
                            image_upload_time = items[i]["image_upload_time"].ToString(),
                            stockiest_code = items[i]["Stockiest_Code"].ToString(),
                            retailar_code = items[i]["RetailerCode"].ToString(),
                            address = items[i]["address"].ToString(),
                            city = items[i]["city"].ToString(),
                            dealer_name = items[i]["dealer_name"].ToString(),
                            drug_license = items[i]["drug_license"].ToString(),
                            gst = items[i]["gst"].ToString(),
                            pin = items[i]["pin"].ToString(),
                            state = items[i]["state"].ToString(),
                            storeName = items[i]["storeName"].ToString(),
                            storeId = items[i]["storeId"].ToString(),
                            storeDescription = items[i]["storeDescription"].ToString()
                        };

                        if (cls.stockiest_code.ToLower() == "other")
                        {
                            cls.storeName = items[i]["dealer_name"].ToString();
                            cls.storeDescription = string.Format(@"{0} {1} {2} {3}", cls.address, cls.city, cls.state, cls.pin);
                        }
                        clslist.Add(cls);
                    }
                }
                roi.image_invoice = clslist;
                roi.status = status;
                ri.root = roi;
                status.code = 200;
                status.success = true;
                status.message = "success";
            }
            else
            {
                roi.status = status;
                ri.root = roi;
                status.code = 500;
                status.success = false;
                status.message = "failure";
            }
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize = PageSize,
                currentPage = CurrentPage,
                totalPages = TotalPages,
                previousPage,
                nextPage
            };
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            ri.count = count;
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            res1.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            return res1;
        }

        [Route("v1.1/imageid/get")]
        [CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage GetImageId(string imageid = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            DataTable dt = gi.GetDataMapping(string.Format(@"{1} (Image_Status in ('OPEN', 'PENDING') or Image_Status like 'ACCEPTED%') and Image_id='{0}' order by image_upload_time", imageid, ConfigurationManager.AppSettings["Invoice_Image"]),
                connectionString);
            RooObjectGetS3URL ri;
            Status status = new Status();
            string d1 = "";
            if (dt.Rows.Count > 0)
            {
                d1 = apiCall.MakeRequest(string.Format(@"{0}{1}.png?orgName={2}", getS3ApiUrl, imageid, brandName), auth);
                logger.Debug(d1);
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RooObjectGetS3URL));
                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d1)))
                {
                    ri = (RooObjectGetS3URL)deserializer.ReadObject(ms);
                    HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
                    res1.Headers.Add("request_Id", request_Id);
                    return res1;
                }
            }
            else
            {
                ri = new RooObjectGetS3URL
                {
                    data = ""
                };
                status.success = false;
                status.message = "URL Not Found for the Image";
                status.code = 500;
                ri.status = status;

            }
            string jn1 = JsonConvert.SerializeObject(status, Formatting.Indented);
            HttpResponseMessage res2 = Request.CreateResponse(HttpStatusCode.OK, status);
            res2.Headers.Add("request_Id", request_Id);
            return res2;
        }

        #region Offer Accepted API
        [HttpGet]
        [Route("v1.1/offer-accepted/get")]
        //[CacheOutput(ClientTimeSpan = 120, ServerTimeSpan = 120)]
        public HttpResponseMessage OfferAccepted(string image_id = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            RootObjectOfferAccepted ri = new RootObjectOfferAccepted();
            RootOfferAccepted roi = new RootOfferAccepted();
            List<OfferAccepted> rr = new List<OfferAccepted>();
            IEnumerable<FailurePrudct> failure = null;
            DataTable d1 = mfg;
            DataTable d2 = pmapping;
            var transNumber = string.Empty;
            try
            {
                IEnumerable<Models.Ocr.Data.HarmonizatedDate> d3 = null;
                if (!string.IsNullOrEmpty(image_id))
                {
                    Models.Ocr.Data.OcrData d = GetOcrData(image_id);
                    if (d != null && d.Bill_info != null)
                    {
                        foreach (Models.Ocr.Data.BillInfo items in d.Bill_info)
                        {
                            roi.transaction_number = items.InvoiceNo;
                            transNumber = items.InvoiceNo;
                            roi.transaction_date = items.InvoiceDate;
                            roi.retailer_present = items.retailer_present;
                            roi.stockist_present = items.stockist_present;
                        }
                    }
                    if (d != null && d.table != null && d1 != null & d2 != null)
                    {
                        d3 = Dataharmonization(d, d1, d2);
                        failure = from t3 in d.table.AsEnumerable()
                                  join t4 in d3.AsEnumerable()
                                  on t3.product equals t4.product
                                  select new FailurePrudct
                                  {
                                      manufacturer = t3.manufacturer,
                                      product = t3.product,
                                      quantity = t3.quantity,
                                      item_code = string.IsNullOrEmpty(t4.item_code) ? string.Empty : t4.item_code
                                  };
                    }
                }
                DataTable dt = pmaster;
                if (d3 != null)
                {
                    try
                    {
                        #region Get GSK Product
                        IOrderedEnumerable<OfferAccepted> result = (from t in dt.AsEnumerable()
                                                                    join t1 in d3
                                                                    on t.Field<string>("item_code") equals t1.item_code
                                                                    into gj
                                                                    from subnet in gj.DefaultIfEmpty()
                                                                    select new OfferAccepted
                                                                    {
                                                                        item_code = t.Field<string>("item_code") == null ? string.Empty : t.Field<string>("item_code"),
                                                                        item_description = t.Field<string>("item_description") == null ? string.Empty : t.Field<string>("item_description"),
                                                                        net_selling_price = t.Field<string>("net_selling_price") == null ? string.Empty : t.Field<string>("net_selling_price"),
                                                                        pack_size = t.Field<string>("pack_size") == null ? string.Empty : t.Field<string>("pack_size"),
                                                                        qty = subnet == null ? "0" : subnet.quantity,
                                                                        offer_accepted = "1"
                                                                    }).OrderByDescending(x => x.qty);
                        roi.offers_accepted = result.GroupBy(x=>x.item_code).Select(g=>g.First()).ToList();
                        #endregion
                    }

                    catch (Exception ex)
                    {
                        d3 = null;
                        logger.Error(ex.ToString());
                    }
                }
                if (d3 == null)
                {
                    OrderedEnumerableRowCollection<OfferAccepted> result = (from t in dt.AsEnumerable()
                                                                            select new OfferAccepted
                                                                            {
                                                                                item_code = t.Field<string>("item_code"),
                                                                                item_description = t.Field<string>("item_description"),
                                                                                net_selling_price = t.Field<string>("net_selling_price"),
                                                                                pack_size = t.Field<string>("pack_size"),
                                                                                qty = "0",
                                                                                offer_accepted = "1"
                                                                            }).OrderByDescending(x => x.qty);
                    roi.offers_accepted = result.GroupBy(x => x.item_code).Select(g => g.First()).ToList();
                }

                #region Old Code
                //var count = 0;
                //if (dt.Rows.Count > 0)
                //{
                //    count = dt.Rows.Count;
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        OfferAccepted cls = new OfferAccepted();
                //        cls.item_code = dt.Rows[i]["item_code"].ToString();
                //        cls.item_description = dt.Rows[i]["item_description"].ToString();
                //        cls.qty = Convert.ToInt32(dt.Rows[i]["Qty"]);
                //        cls.net_selling_price = dt.Rows[i]["net_selling_price"].ToString();
                //        cls.pack_size = dt.Rows[i]["pack_size"].ToString();
                //        cls.offer_accepted = dt.Rows[i]["offer_accepted"].ToString();
                //        rr.Add(cls);
                //    }
                //    roi.offers_accepted = rr;
                //    ri.root = roi;
                //}
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            ri.root = roi;
            ri.count = roi.offers_accepted.Count;
            roi.offers_accepted=roi.offers_accepted.Distinct().ToList();
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            //logger.Debug(jn1);
            if (failure != null)
            {
                Thread thread = new Thread(() => LogFailureMaping(failure, image_id, transNumber));
                thread.Start();
            }
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        private Models.Ocr.Data.OcrData GetOcrData(string image_id)
        {
            string jobId = string.Empty;
            if (!string.IsNullOrEmpty(image_id))
            {
                string jobIdQuery = string.Format(@"SELECT  `ocr_job_id` FROM  `Invoice_Image` WHERE  `Image_id` = '{0}'  ORDER BY  `ocr_job_id_update_date` DESC  limit 1", image_id);
                DataTable dt = gi.GetDataMapping(jobIdQuery, connectionString);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        jobId = dt.Rows[i]["ocr_job_id"].ToString();
                    }
                }
            }
            Models.Ocr.Data.OcrData ocr = null;
            if (!string.IsNullOrEmpty(jobId))
            {
                string[] getOcrJson = Directory.GetFiles(@"C:\GSK\OcrData", string.Format("{1}_{0}.json", jobId, image_id));
                foreach (string file in getOcrJson)
                {
                    string OcrData = File.ReadAllText(file);
                    if (!string.IsNullOrEmpty(OcrData))
                    {
                        OcrData = OcrData.Replace("Bill info", "Bill_info");

                        try
                        {
                            DataContractJsonSerializer custdes = new DataContractJsonSerializer(typeof(Models.Ocr.Data.OcrData));
                            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(OcrData)))
                            {
                                ocr = (Models.Ocr.Data.OcrData)custdes.ReadObject(ms);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.ToString());
                        }
                    }
                }
            }
            if (ocr != null && ocr.table != null && ocr.table.Count > 0)
            {

            }
            return ocr;
        }
        private IEnumerable<Models.Ocr.Data.HarmonizatedDate> Dataharmonization(Models.Ocr.Data.OcrData ocr, DataTable mfg, DataTable pMT)
        {
            if (mfg.Rows.Count > 0 && pMT.Rows.Count > 0)
            {
                if (ocr != null && ocr.table != null && ocr.table.Count > 0)
                {
                    IEnumerable<Models.Ocr.Data.HarmonizatedDate> query = from i in ocr.table.AsEnumerable()
                                                                          join i2 in mfg.AsEnumerable()
                                                                          on i.manufacturer equals i2.Field<string>("Manufacture_Name")
                                                                          into gj
                                                                          from subnet in gj.DefaultIfEmpty()
                                                                          join i3 in pMT.AsEnumerable() on i.product equals i3.Field<string>("POSSIBLE_NAMES_IN_POS")
                                                                          into gj1
                                                                          from subnet1 in gj1.DefaultIfEmpty()
                                                                          select new Models.Ocr.Data.HarmonizatedDate
                                                                          {
                                                                              product = i.product,
                                                                              quantity = i.quantity,
                                                                              mfg_name = subnet == null ? string.Empty : subnet.Field<string>("Manufacture_Name"),
                                                                              item_code = subnet1 == null ? string.Empty : subnet1.Field<string>("GSK_SKU_CODE"),
                                                                              item_desc = subnet1 == null ? string.Empty : subnet1.Field<string>("GSK_SKU_NAME")
                                                                          };
                    return query;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        private void LogFailureMaping(IEnumerable<FailurePrudct> failure, string image_id, string transNumber)
        {
            if (failure.Count() > 0)
            {
                string stockiestCode = string.Empty;
                string retailerCode = string.Empty;
                var dtable = gi.GetDataMapping(string.Format(@"select RetailerCode,Stockiest_Code from  Invoice_Image where Image_id='{0}'", image_id), connectionString);
                if (dtable != null && dtable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtable.Rows)
                    {
                        stockiestCode = dr["Stockiest_Code"].ToString();
                        retailerCode = dr["RetailerCode"].ToString();
                    }
                }
                try
                {
                    foreach (var items in failure)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(items.item_code) && (items.manufacturer.StartsWith("G") || string.IsNullOrEmpty(items.manufacturer)))
                            {
                                using (MySqlConnection connection = new MySqlConnection(connectionString))
                                {
                                    connection.Open();
                                    MySqlCommand mycommand = new MySqlCommand("sp_sku_failure_ocr", connection)
                                    {
                                        CommandType = CommandType.StoredProcedure
                                    };
                                    mycommand.Parameters.AddWithValue("@Stockiest_SKU_Name", string.IsNullOrEmpty(items.product) ? string.Empty : items.product);
                                    mycommand.Parameters.AddWithValue("@Invoice_No", string.IsNullOrEmpty(transNumber) ? string.Empty : transNumber);
                                    mycommand.Parameters.AddWithValue("@Invoice_Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                    mycommand.Parameters.AddWithValue("@Failure_Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                    mycommand.Parameters.AddWithValue("@Stockiest_Code", stockiestCode);
                                    mycommand.Parameters.AddWithValue("@Mapping_Date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                    mycommand.Parameters.AddWithValue("@RetailerCode", retailerCode);
                                    mycommand.Parameters.AddWithValue("@qty", items.quantity);
                                    mycommand.Parameters.AddWithValue("@Image_id", image_id);
                                    mycommand.Parameters.AddWithValue("@Mapping_Flag", "0");
                                    try
                                    {

                                        mycommand.ExecuteNonQuery();
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex.ToString());
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
        }
        #endregion

        [Route("v1.1/transaction-intouch/add")]
        public HttpResponseMessage IntouchTransactionAdd(RootObject root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            string billNumber, retailerCode;
            #region Local Variables
            StringBuilder sb = new StringBuilder();
            Status status = new Status();
            TransactionResponse tresponse = new TransactionResponse();
            ItemStatus itemStatus = new ItemStatus();
            LineItem lineitem = new LineItem();
            Transactions transRes = new Transactions();
            RootObjectAddResponse rootRes = new RootObjectAddResponse();
            Response res = new Response();
            List<TransactionResponse> listTresponse = new List<TransactionResponse>();
            string filename = string.Empty;
            string stockistcode = string.Empty;
            var imageId = string.Empty;
            #endregion
            try
            {
                if (root != null)
                {

                    string json2 = JsonConvert.SerializeObject(root, Formatting.Indented);
                    logger.Debug("Request " + json2);


                    foreach (Transaction trans in root.root.transaction)
                    {

                        foreach (var cf in trans.custom_fields.field)
                        {
                            if (cf.name == "image_id")
                            {
                                imageId = cf.value;
                            }
                        }

                        bool flag = true;
                        string requestType = trans.type;
                        tresponse.billing_time = trans.billing_time;
                        tresponse.entered_by = trans.entered_by;
                        string customer_id = trans.customer.external_id;
                        string number = trans.number;
                        billNumber = number;
                        retailerCode = customer_id;
                        bool checkDate = DateTime.TryParse(trans.billing_time, out DateTime date);
                        if (string.IsNullOrEmpty(customer_id))
                        {
                            flag = false;
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Customer external id is blank/invalid";
                            itemStatus.code = 400;
                        }
                        else if (!checkDate && flag)
                        {
                            flag = false;
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Transaction Date is not correct";
                            itemStatus.code = 400;
                        }
                        else if (requestType != "regular" && requestType.ToLower() != "return" && flag)
                        {
                            flag = false;
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Transaction Type is not correct";
                            itemStatus.code = 400;
                        }
                        else if (string.IsNullOrEmpty(number) && flag)
                        {
                            flag = false;
                            status.success = false;
                            status.message = "Request is not in Proper format";
                            status.code = 500;

                            itemStatus.success = false;
                            itemStatus.message = "Transaction number is not correct";
                            itemStatus.code = 400;
                        }
                        else
                        {
                            double transAmount = Validation.AmountCheck(trans.amount);
                            if (transAmount < 0 && flag)
                            {
                                flag = false;
                                status.success = false;
                                status.message = "Request is not in Proper format";
                                status.code = 500;

                                itemStatus.success = false;
                                itemStatus.message = "Transaction Amount is not correct";
                                itemStatus.code = 400;
                            }
                            else if (!string.IsNullOrEmpty(trans.gross_amount) && flag)
                            {
                                double transGross = Validation.AmountCheck(trans.gross_amount);
                                if (transGross < 0)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "Transaction Gross Amount is not correct";
                                    itemStatus.code = 400;

                                }
                            }
                            else if (!string.IsNullOrEmpty(trans.discount) && flag)
                            {
                                double transDiscount = Validation.AmountCheck(trans.discount);
                                if (transDiscount < 0)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "Transaction Discount is not correct";
                                    itemStatus.code = 400;

                                }
                            }
                        }

                        if (flag)
                        {
                            foreach (LineItem line in trans.line_items.line_item)
                            {
                                double rate = Validation.AmountCheck(line.rate);
                                double value = Validation.AmountCheck(line.value);
                                double amount = Validation.AmountCheck(line.amount);
                                double discount = Validation.AmountCheck(line.discount);
                                double qty = Validation.IntCheck(line.qty);

                                if (rate < 0 && flag)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item Rate is not correct";
                                    itemStatus.code = 400;
                                }
                                else if (value < 0 && flag)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item Value is not correct";
                                    itemStatus.code = 400;

                                }
                                else if (amount < 0 && flag)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item amount is not correct";
                                    itemStatus.code = 400;
                                }
                                else if (discount < 0 && flag)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item discount is not correct";
                                    itemStatus.code = 400;

                                }
                                else if (qty < 0 && flag)
                                {
                                    flag = false;
                                    status.success = false;
                                    status.message = "Request is not in Proper format";
                                    status.code = 500;

                                    itemStatus.success = false;
                                    itemStatus.message = "line Item qty is not correct";
                                    itemStatus.code = 400;

                                }
                            }
                        }
                        if (flag)
                        {
                            if (!trans.entered_by.StartsWith("OTHER"))
                            {
                                iunid = string.Format("{0}{1}", trans.entered_by, appendingNumberTill);
                            }
                            else
                            {
                                string customerGetAPI = string.Format(@"{1}&external_id={0}", customer_id, customerGetApi);
                                string custResponse = apiCall.IntouchMakeRequest(customerGetAPI, "", liveOrgTillId, iunPwd, trans.billing_time, "");
                                DataContractJsonSerializer custdes = new DataContractJsonSerializer(typeof(RootObjectCustomer));
                                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(custResponse)))
                                {
                                    RootObjectCustomer rcust = (RootObjectCustomer)custdes.ReadObject(ms);
                                    foreach (Customer cust in rcust.response.customers.customer)
                                    {
                                        trans.customer.mobile = cust.mobile;
                                        trans.customer.email = cust.email;
                                        trans.customer.firstname = cust.firstname;
                                        trans.customer.lastname = cust.lastname;
                                    }
                                }

                            }
                            logger.Debug("Capillary Till ID " + iunid);
                            trans.entered_by = iunid;
                            json2 = JsonConvert.SerializeObject(root, Formatting.Indented);

                            string d = apiCall.IntouchMakeRequest(intouchTrasnactionAddApi, json2, iunid, iunPwd, trans.billing_time, imageId);
                            try
                            {
                                logger.Debug("Entered By " + trans.entered_by);
                                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RootObjectAddResponse));
                                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d)))
                                {
                                    RootObjectAddResponse ri = (RootObjectAddResponse)deserializer.ReadObject(ms);
                                    foreach (var transaction in ri.response.transactions.transaction)
                                    {
                                        if (transaction.item_status.code == 604)
                                        {
                                            RootObjectAddResponse rio = multiPageInvoiceHandling.ValidateProduct(json2, billNumber, retailerCode, iunid, imageId);
                                            if (rio.response != null)
                                            {
                                                ri = rio;
                                            }
                                        }
                                    }

                                    return Request.CreateResponse(HttpStatusCode.OK, ri);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.ToString());
                                flag = false;
                                status.success = false;
                                status.message = "The remote server returned an error: (401) Unauthorized.";
                                status.code = 500;

                                itemStatus.success = false;
                                itemStatus.message = "The remote server returned an error: (401) Unauthorized.";
                                itemStatus.code = 400;
                                tresponse.item_status = itemStatus;
                                listTresponse.Add(tresponse);
                                transRes.transaction = listTresponse;
                                res.status = status;
                                res.transactions = transRes;
                                rootRes.response = res;

                            }
                        }
                        else
                        {
                            res.status = status;
                            tresponse.item_status = itemStatus;
                            listTresponse.Add(tresponse);
                            transRes.transaction = listTresponse;
                            res.status = status;
                            res.transactions = transRes;
                            rootRes.response = res;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                #region Failure Response
                status.success = false;
                status.message = ex.Message.ToString();
                status.code = 500;

                itemStatus.success = false;
                itemStatus.message = ex.Message.ToString();
                itemStatus.code = 400;
                res.status = status;
                tresponse.item_status = itemStatus;
                listTresponse.Add(tresponse);
                transRes.transaction = listTresponse;
                res.status = status;
                res.transactions = transRes;
                rootRes.response = res;
                #endregion
            }
            string jn = JsonConvert.SerializeObject(rootRes, Formatting.Indented);
            logger.Debug(jn.ToString());
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootRes);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/image/update")]
        public HttpResponseMessage UpdateInvoiceImage(RootObjectImageInvoice root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            var imageStatus = string.Empty;
            var imageId = string.Empty;
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string jn1 = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn1);
            try
            {
                if (root != null)
                {
                    foreach (ImageInvoice trans in root.root.image_invoice)
                    {
                        string json2 = JsonConvert.SerializeObject(trans, Formatting.None);
                        logger.Debug(json2);
                        if (string.IsNullOrEmpty(trans.image_id) ||
                                string.IsNullOrEmpty(trans.image_update_time) || string.IsNullOrEmpty(trans.updated_by))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "All requests have failed due to errors";
                        }
                        else
                        {
                            bool flag = true;
                            try
                            {
                                DateTime d1 = Convert.ToDateTime(trans.image_upload_time);
                            }
                            catch (Exception)
                            {
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "All requests have failed due to errors";
                            }
                            if (flag)
                            {
                                using (MySqlConnection connection = new MySqlConnection(connectionString))
                                {
                                    connection.Open();
                                    MySqlCommand mycommand = new MySqlCommand("sp_image_invoice_update", connection)
                                    {
                                        CommandType = CommandType.StoredProcedure
                                    };
                                    mycommand.Parameters.AddWithValue("@P_Image_id", trans.image_id.ToString());
                                    mycommand.Parameters.AddWithValue("@P_Image_Status", trans.image_status.ToString());
                                    mycommand.Parameters.AddWithValue("@P_status_update_time", Validation.IST().ToString("yyyy-MM-dd HH:mm:ss"));
                                    mycommand.Parameters.AddWithValue("@P_updated_by", trans.updated_by.ToString());
                                    mycommand.Parameters.AddWithValue("@P_transaction_number", trans.transaction_number.ToString());
                                    try
                                    {
                                        imageStatus = trans.image_status;
                                        imageId = trans.image_id;
                                        mycommand.ExecuteNonQuery();
                                        status.code = 200;
                                        status.success = true;
                                        status.message = "invoice image information updated successfully";
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex.ToString());

                                        status.code = 500;
                                        status.success = false;
                                        status.message = "All requests have failed due to errors";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.success = false;
                status.message = "All requests have failed due to errors";
            }
            finally
            {
                if (rejectedInvoiceToCrm == "1")
                {
                    Thread thread = new Thread(() => RejectInvoiceToCRM(imageStatus, imageId, status));
                    thread.Start();
                }
            }
            response.status = status;
            rootResponse.response = response;
            string jn = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
            logger.Debug(jn);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        private void RejectInvoiceToCRM(string imageStatus, string imageId, Status status)
        {
            try
            {
                if (status.code == 200 && imageStatus.ToLower().StartsWith(@"rejected"))
                {
                    var stockiestCode = string.Empty;
                    var retailerCode = string.Empty;
                    RootObject request = new RootObject();
                    Transaction transaction = new Transaction();
                    List<Transaction> transactions = new List<Transaction>();
                    Customer customer = new Customer();
                    Field field = new Field();
                    List<Field> fields = new List<Field>();
                    ExtendedFields extendedFields = new ExtendedFields();
                    Root root1 = new Root();
                    var q = string.Format(@"select * from Invoice_Image where Image_id='{0}'", imageId);
                    var dt = gi.GetDataMapping(q, connectionString);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            retailerCode = dt.Rows[i]["RetailerCode"].ToString();
                            stockiestCode = dt.Rows[i]["Stockiest_Code"].ToString();
                        }
                    }
                    //retailerCode = "ABCD12348";
                    customer.external_id = retailerCode;
                    transaction.number = imageId;
                    transaction.amount = "1";
                    transaction.notes = imageStatus;
                    transaction.type = "regular";
                    field.name = "rejectionreason";
                    fields.Add(field);
                    extendedFields.field = fields;
                    field.value = imageStatus;
                    transaction.custom_fields = extendedFields;
                    transaction.billing_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    transaction.customer = customer;
                    transactions.Add(transaction);
                    root1.transaction = transactions;
                    request.root = root1;
                    string re = JsonConvert.SerializeObject(request, Formatting.Indented);
                    //stockiestCode = @"gsk.jkmedico.7";
                    if (stockiestCode.StartsWith("OTHER"))
                    {
                        stockiestCode = "others";
                    }

                    string d = apiCall.IntouchMakeRequest(intouchTrasnactionAddApi, re, string.Format("{0}{1}", stockiestCode,rejectedTillAppned), iunPwd, "", imageId);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        [HttpGet]
        [Route("v1.1/imageid/validate")]
        //[CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage ValidateImageId(string imageId = "", string billNumber = "")
        {
            RootResponse rootResponse = new RootResponse();
            RooObjectGetS3URL s3_response = new RooObjectGetS3URL();
            List<TransTransaction> transaction = new List<TransTransaction>();
            Status status = new Status();
            var retailerId = string.Empty;
            var stockiestCode = string.Empty;
            var tillid = string.Empty;
            var transactionGetAPI = string.Empty;
            ImageInvoice imageInvoice = new ImageInvoice();
            if (!string.IsNullOrEmpty(imageId))
            {
                try
                {
                    var q = string.Format(@"{1} where Image_id='{0}' order by image_upload_time", imageId, ConfigurationManager.AppSettings["invoiceImageHistoryQuery"]);
                    logger.Debug(q);
                    DataTable dt = gi.GetDataMapping(q, connectionString);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            retailerId = dt.Rows[i]["RetailerCode"].ToString();
                            stockiestCode = dt.Rows[i]["Stockiest_Code"].ToString();
                            imageInvoice.address = dt.Rows[i]["address"].ToString();
                            imageInvoice.city = dt.Rows[i]["city"].ToString();
                            imageInvoice.dealer_name = dt.Rows[i]["dealer_name"].ToString();
                            imageInvoice.drug_license = dt.Rows[i]["drug_license"].ToString();
                            imageInvoice.gst = dt.Rows[i]["gst"].ToString();
                            imageInvoice.id = dt.Rows[i]["SerialNo"].ToString();
                            imageInvoice.image_id = dt.Rows[i]["image_id"].ToString();
                            imageInvoice.image_status = dt.Rows[i]["image_status"].ToString();
                            imageInvoice.image_update_time = dt.Rows[i]["status_update_time"].ToString();
                            imageInvoice.image_upload_time = dt.Rows[i]["image_upload_time"].ToString();
                            imageInvoice.pin = dt.Rows[i]["pin"].ToString();
                            imageInvoice.retailar_code = dt.Rows[i]["RetailerCode"].ToString();
                            imageInvoice.state = dt.Rows[i]["state"].ToString();
                            imageInvoice.stockiest_code = dt.Rows[i]["Stockiest_Code"].ToString();
                            imageInvoice.storeDescription = dt.Rows[i]["storeDescription"].ToString();
                            imageInvoice.storeId = dt.Rows[i]["storeId"].ToString();
                            imageInvoice.storeName = dt.Rows[i]["storeName"].ToString();
                            imageInvoice.transaction_number = dt.Rows[i]["transaction_number"].ToString();
                            imageInvoice.updated_by = dt.Rows[i]["updated_by"].ToString();

                            if (dt.Rows[i]["transaction_number"].ToString().Trim() != "N/A")
                            {
                                billNumber = dt.Rows[i]["transaction_number"].ToString();
                            }
                        }
                        rootResponse.image_invoice = imageInvoice;
                        var d1 = apiCall.MakeRequest(string.Format(@"{0}{1}.png?orgName={2}", getS3ApiUrl, imageId, brandName), auth);
                        logger.Debug(d1);
                        DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RooObjectGetS3URL));
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d1)))
                        {
                            var ri = (RooObjectGetS3URL)deserializer.ReadObject(ms);
                            if (ri.data != null)
                            {
                                s3_response = ri;
                            }
                            else
                            {
                                RooObjectGetS3URL rooObjectGetS3URL = new RooObjectGetS3URL();
                                Status s = new Status();
                                s.code = 500;
                                s.message = "Image Not exists";
                                s.success = false;
                                rooObjectGetS3URL.status = s;
                                s3_response = rooObjectGetS3URL;
                            }
                        }
                    }
                    else
                    {
                        RooObjectGetS3URL rooObjectGetS3URL = new RooObjectGetS3URL();
                        Status s = new Status();
                        s.code = 500;
                        s.message = "Image Not exists";
                        s.success = false;
                        rooObjectGetS3URL.status = s;
                        s3_response = rooObjectGetS3URL;
                    }
                    rootResponse.s3_response = s3_response;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }

            if (!string.IsNullOrEmpty(billNumber) && !string.IsNullOrEmpty(retailerId))
            {
                try
                {
                    if (billNumber.Contains("_"))
                    {
                        var a = billNumber.Split('_');
                        billNumber = a[0];
                    }
                    else if(billNumber.Contains("!"))
                    {
                        var a = billNumber.Split('!');
                        billNumber = a[0];
                    }
                    var query = string.Format("{0} '{1}%' and Retailer_Code='{2}'", getBillNumber, billNumber, retailerId);

                    if (stockiestCode == "OTHER")
                    {
                        //query = string.Format("{0} '{1}%' and Retailer_Code='{2}' and Stockiest_Code like '{3}%'", getBillNumber, billNumber, retailerId, iunid);
                        tillid = iunid;
                    }
                    else
                    {
                        //query = string.Format("{0} '{1}%' and Retailer_Code='{2}'", getBillNumber, billNumber, retailerId, stockiestCode);
                        tillid = liveOrgTillId;
                    }
                    logger.Debug(query);
                    DataTable dt = gi.GetDataMapping(query, connectionString);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var transactionNumber = dt.Rows[i]["Bill_Number"].ToString();
                            retailerId = dt.Rows[i]["Retailer_Code"].ToString();
                            transactionGetAPI = string.Format(@"{0}&external_id={2}&number={1}&type=ALL", customerTrasnactionGetApi, transactionNumber, retailerId);
                            string liveOrgTransGetResponse = apiCall.IntouchMakeRequest(transactionGetAPI, "", tillid, "gsk123", "", "");
                            TransRootObject ri = new TransRootObject();
                            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(TransRootObject));
                            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(liveOrgTransGetResponse)))
                            {
                                ri = (TransRootObject)deserializer.ReadObject(ms);
                                foreach (var t in ri.response.customer.transactions.transaction)
                                {
                                    foreach (var line in t.line_items.line_item)
                                    {
                                        if (line.type.ToUpper() == "RETURN")
                                        {
                                            line.qty = string.Format("-{0}", line.qty);
                                            line.amount = string.Format("-{0}", line.amount);
                                            line.rate = string.Format("-{0}", line.rate);
                                            line.value = string.Format("-{0}", line.value);
                                            var desc = from t1 in pmaster.AsEnumerable()
                                                       where t1.Field<string>("item_code") == line.item_code
                                                       select new
                                                       {
                                                           item_desc = t1.Field<string>("item_description")
                                                       };
                                            foreach (var d in desc)
                                            {
                                                line.description = d.item_desc;
                                            }
                                        }
                                    }
                                    transaction.Add(t);
                                }

                            }
                        }
                    }

//                    List<ImageHistroy> imageHistroys = new List<ImageHistroy>();
//                    var query1 = string.Format(@"SELECT distinct T1.image_id, T2.Bill_Number transaction_number, T1.updated_by, T1.Image_Status FROM `image_history` T1
//join
//API_Response T2
//on
//T1.image_id=T2.Image_id " +
//                        "where T1.transaction_number like '{0}%' and T1.retailer_code='{1}' " +
//                        "and T1.image_id = '{2}'", billNumber, retailerId, imageId);
//                    logger.Debug(query1);
//                    DataTable dt1 = gi.GetDataMapping(query1, connectionString);
//                    if (dt1.Rows.Count > 0)
//                    {
//                        StringBuilder sb = new StringBuilder();
//                        for (int i = 0; i < dt1.Rows.Count; i++)
//                        {
//                            ImageHistroy imageHistroy = new ImageHistroy();
//                            imageHistroy.image_id= dt1.Rows[i]["image_id"].ToString();
//                            imageHistroy.invoice_number = dt1.Rows[i]["transaction_number"].ToString();
//                            imageHistroy.updated_at = getdate(connectionString, string.Format(@"SELECT `image_update_time` FROM `image_history` 
//where `image_id`='{0}' and `retailer_code`='{1}'
//", imageId, retailerId));
//                            imageHistroy.updated_by = dt1.Rows[i]["updated_by"].ToString();
//                            imageHistroy.status = dt1.Rows[i]["Image_Status"].ToString();
//                            imageHistroys.Add(imageHistroy);
//                        }
//                        rootResponse.imageHistroys = imageHistroys;
//                        //imageInvoice.image_status = string.Format(@"{2}{1}", imageInvoice.image_status, sb.ToString(), Environment.NewLine);
//                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }

            else if (!string.IsNullOrEmpty(billNumber) && string.IsNullOrEmpty(retailerId))
            {
                transactionGetAPI = string.Format(@"{0}&number={1}&type=REGULAR", getBillNumberApi, billNumber, retailerId);
                string liveOrgTransGetResponse = apiCall.IntouchMakeRequest(transactionGetAPI, "", liveOrgTillId, "gsk123", "", "");
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(TransRootObject));
                TransRootObject ri = new TransRootObject();
                var flag = false;
                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(liveOrgTransGetResponse)))
                {
                    ri = (TransRootObject)deserializer.ReadObject(ms);
                    foreach (var t in ri.response.transactions.transaction)
                    {
                        transaction.Add(t);
                        if (t.line_items == null)
                        {
                            flag = true;
                        }
                    }
                }
                try
                {
                    if (flag)
                    {
                        liveOrgTransGetResponse = apiCall.IntouchMakeRequest(transactionGetAPI, "", iunid, "gsk123", "", "");
                        deserializer = new DataContractJsonSerializer(typeof(TransRootObject));
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(liveOrgTransGetResponse)))
                        {
                            ri = (TransRootObject)deserializer.ReadObject(ms);
                            foreach (var t in ri.response.transactions.transaction)
                            {
                                transaction.Add(t);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }

            try
            {
                List<ImageHistroy> imageHistroys = new List<ImageHistroy>();
                var query1 = string.Format(@"SELECT * FROM `image_history` where `image_id`='{2}'", billNumber, retailerId, imageId);
                logger.Debug(query1);
                DataTable dt1 = gi.GetDataMapping(query1, connectionString);
                if (dt1.Rows.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        ImageHistroy imageHistroy = new ImageHistroy();
                        imageHistroy.image_id = dt1.Rows[i]["image_id"].ToString();
                        imageHistroy.invoice_number = dt1.Rows[i]["transaction_number"].ToString();
                        imageHistroy.updated_at = dt1.Rows[i]["datetime"].ToString();
                        imageHistroy.updated_by = dt1.Rows[i]["updated_by"].ToString();
                        imageHistroy.status = dt1.Rows[i]["Image_Status"].ToString();
                        imageHistroys.Add(imageHistroy);
                    }
                    rootResponse.imageHistroys = imageHistroys;
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
            }
            transaction.RemoveAll(x => x.line_items == null);

            if ((s3_response.data != null && s3_response.status != null) || transaction.Count > 0)
            {
                status.code = 200;
                status.message = "SUCCESS";
                status.success = true;
            }
            else
            {
                status.code = 500;
                status.message = "FAILURE";
                status.success = false;
            }
            rootResponse.transaction = transaction;
            rootResponse.status = status;
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            //res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/stockiest/get")]
        public HttpResponseMessage GetStockiest(string stockiestname)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            RootStockiestMapping rootStockiestMapping = new RootStockiestMapping();
            List<StockiestMapping> stockiestMappings = new List<StockiestMapping>();
            Status status = new Status();
            if (!string.IsNullOrEmpty(stockiestname))
            {
                try
                {
                    DataRow[] drtable = stockiestMapping.Select(string.Format("stockiest_name like '%{0}%'", stockiestname));
                    if (drtable.Count() > 0)
                    {
                        foreach (var dr in drtable)
                        {
                            StockiestMapping stockiestMapping = new StockiestMapping();
                            stockiestMapping.name = dr["stockiest_name"].ToString();
                            stockiestMapping.gst_no = dr["gst_no"].ToString();
                            stockiestMapping.street = dr["street"].ToString();
                            stockiestMapping.city = dr["city"].ToString();
                            stockiestMapping.pin_code = (int)dr["pin_code"];
                            stockiestMapping.stockiest_code = dr["stockiest_code"].ToString();
                            stockiestMappings.Add(stockiestMapping);
                        }
                        status.code = 200;
                        status.success = true;
                        status.message = "Success";
                    }
                    else
                    {
                        status.code = 500;
                        status.success = false;
                        status.message = "Failure";
                    }
                }
                catch(Exception ex)
                {
                    logger.Error(ex.ToString());
                    status.code = 500;
                    status.success = false;
                    status.message = "Failure";
                }
            }

            rootStockiestMapping.stockiest_mapping = stockiestMappings;
            rootStockiestMapping.status = status;
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootStockiestMapping);
            res1.Headers.Add("request_Id", request_Id);
            return res1;

        }
        #endregion


        public string getdate(string connString, string sqlQuery)
        {
            string value = "";
            MySqlConnection conn = new MySqlConnection(connString);
            MySqlCommand cmd = new MySqlCommand(sqlQuery, conn);
            try
            {
                conn.Open();
                value = Convert.ToDateTime(cmd.ExecuteScalar()).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return value;
        }
    }
}