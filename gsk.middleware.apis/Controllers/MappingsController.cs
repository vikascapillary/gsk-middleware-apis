﻿using gsk.middleware.apis.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;


namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class MappingsController : ApiController
    {
        #region Variables
        private Logger logger;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private ApiCall apiCall = ApiCall.GetInstance;
        private readonly string connectionString, skuMappingQuery, stockiestQuery, skuMappingItemDescQuery, retailerMappingQuery;
        string productGetApi;
       
        private string iunid;
        private readonly string iunPwd;
        private readonly string liveOrgTillId;
        private readonly DataTable rejectReason;
        static object rejectReasonTable = new object();
        const string rejectReasonCache = "rejectReasonCache";

        #endregion
        public MappingsController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            GetMappings.Mappings();
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
           connectionString = string.Format("{0}&e;", connectionString);
            skuMappingQuery = ConfigurationManager.AppSettings["SkuMappingQuery"];
            stockiestQuery = ConfigurationManager.AppSettings["StockiestQuery"];
            skuMappingItemDescQuery = ConfigurationManager.AppSettings["SkuMappingItemDescQuery"];
            retailerMappingQuery = ConfigurationManager.AppSettings["RetailerMappingQuery"];
            productGetApi = ConfigurationManager.AppSettings["IntouchProductGetAPI"];
            iunid = ConfigurationManager.AppSettings["IunId"];
            iunPwd = ConfigurationManager.AppSettings["IunPwd"];
            liveOrgTillId = ConfigurationManager.AppSettings["LiveOrgTillId"];
            rejectReason = GetMappings.rejectReason;
            #endregion
        }

        #region Get Mapping 
        [Route("v1.1/gsk-items/get")]
        [CacheOutput(ClientTimeSpan = 40000, ServerTimeSpan = 40000)]
        public HttpResponseMessage GetGskIdentifierUsingItemDesc(string item_desc = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            RootIdentifierItemDesc ri = new RootIdentifierItemDesc();
            RootObjectIdentifierItemDesc roi = new RootObjectIdentifierItemDesc();
            List<IdentifierItemDesc> clslist = new List<IdentifierItemDesc>();
            DataTable dt = gi.GetDataMapping(string.Format(@"{0} where POSSIBLE_NAMES_IN_POS like '%{1}%'", skuMappingItemDescQuery, item_desc), connectionString);

            #region Code
            try
            {
                if (dt.Rows.Count > 0)
                {
                    DataTable distinctSkus = DistinctSku(dt);
                    List<IntouchPrice> inList = new List<IntouchPrice>();
                    RootIntouchPrice rintouch = new RootIntouchPrice();
                    foreach (DataRow d in distinctSkus.Rows)
                    {
                        IntouchPrice pp = new IntouchPrice();
                        string d1 = d["GSK_SKU_CODE"].ToString();
                        if (!string.IsNullOrEmpty(d1))
                        {
                            string d133 = apiCall.IntouchMakeRequest(string.Format("{0}&sku={1}", productGetApi, d1), "", iunid, iunPwd, "", "");
                            if (d133.Contains(@"Product retrieval unsuccessful"))
                            {
                                d133 = apiCall.IntouchMakeRequest(string.Format("{0}&sku={1}", productGetApi, d1), "", liveOrgTillId, iunPwd, "", "");
                            }
                            if (d133.Contains(@"Product retrieval successful"))
                            {
                                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RootObjectAddResponse));
                                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d133)))
                                {
                                    RootObjectAddResponse response = (RootObjectAddResponse)deserializer.ReadObject(ms);
                                    {
                                        foreach (Item trans in response.response.product.item)
                                        {
                                            pp.item_code = trans.sku.ToString();
                                            pp.price = trans.price.ToString();
                                            pp.desc = trans.description.ToString();
                                            try
                                            {
                                                foreach (Models.Attribute attribute in trans.attributes.attribute)
                                                {
                                                    string ate = attribute.name;
                                                    if (ate == "Pack_Size")
                                                    {
                                                        pp.pack_size = attribute.value;
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error(ex.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            inList.Add(pp);
                        }
                    }
                    rintouch.intouch_price = inList;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        IdentifierItemDesc cls = new IdentifierItemDesc
                        {
                            id = dt.Rows[i]["SerialNo"].ToString(),
                            gsk_code = dt.Rows[i]["GSK_SKU_CODE"].ToString(),
                            gsk_desc = dt.Rows[i]["GSK_SKU_NAME"].ToString(),
                            gsk_pack_size = dt.Rows[i]["PACK_SIZE_INVOICED_BY_GSK"].ToString(),
                            stockiest_item_desc = dt.Rows[i]["POSSIBLE_NAMES_IN_POS"].ToString()
                        };

                        if (rintouch.intouch_price.Count > 0)
                        {
                            foreach (IntouchPrice list in rintouch.intouch_price)
                            {
                                if (list.item_code == cls.gsk_code)
                                {
                                    cls.gsk_code = list.item_code;
                                    cls.gsk_desc = list.desc;
                                    cls.price = list.price;
                                    if (!string.IsNullOrEmpty(list.pack_size))
                                    {
                                        cls.gsk_pack_size = list.pack_size;
                                    }
                                }
                            }
                        }
                        clslist.Add(cls);
                    }

                    ri.gsk_identifiers = clslist;
                    roi.root = ri;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            #endregion

            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/gsk-identifier-mfg/get")]
        [CacheOutput(ClientTimeSpan = 40000, ServerTimeSpan = 40000)]
        public HttpResponseMessage GetGskIdentifierUsingMFG(string mfg_name = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            RootIdentifier ri = new RootIdentifier();
            Status status = new Status();
            RootObjectIdentifier roi = new RootObjectIdentifier();
            List<Identifier> clslist = new List<Identifier>();

            #region Code
            DataTable dt = gi.GetDataMapping(string.Format(@"{0} where Manufacture_Name like '%{1}%'", skuMappingQuery, mfg_name), connectionString);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Identifier cls = new Identifier
                    {
                        id = dt.Rows[i]["serial_No"].ToString(),
                        gsk_identifier = dt.Rows[i]["Manufacture_Name"].ToString()
                    };
                    clslist.Add(cls);
                }
                ri.gsk_identifiers = clslist;
                roi.root = ri;
                status.code = 200;
                status.success = true;
                status.message = "success";
            }
            #endregion

            ri.status = status;
            roi.root = ri;
            string jn1 = JsonConvert.SerializeObject(roi, Formatting.Indented);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, roi);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        private DataTable DistinctSku(DataTable cAnddata)
        {
            DataTable uniqueData = null;
            try
            {
                DataView view = new DataView(cAnddata);
                uniqueData = view.ToTable(true, "GSK_SKU_CODE");
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
            }
            return uniqueData;
        }

        [Route("v1.1/gsk-stockiest/get")]
        [CacheOutput(ClientTimeSpan = 40000, ServerTimeSpan = 40000)]
        public HttpResponseMessage GetStockiest(string stockiest_code = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            DataTable dt = gi.GetDataMapping(string.Format(@"{0} where Stockiest_Code like '%{1}%'", stockiestQuery, stockiest_code), connectionString);
            RootStockiest ri = new RootStockiest();
            RootObjectStockiest roi = new RootObjectStockiest();
            List<Stockiest> clslist = new List<Stockiest>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Stockiest cls = new Stockiest
                    {
                        id = dt.Rows[i]["SerialNo"].ToString(),
                        code = dt.Rows[i]["Stockiest_Code"].ToString(),
                        name = dt.Rows[i]["Stockiest_Name"].ToString(),
                        integration = dt.Rows[i]["Integration"].ToString()
                    };
                    clslist.Add(cls);
                }
                ri.gsk_identifiers = clslist;
                roi.root = ri;
            }
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            //logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [HttpGet]
        [Route("v1.1/rejected-reasons/get")]
        [CacheOutput(ClientTimeSpan = 40000, ServerTimeSpan = 40000)]
        public HttpResponseMessage RejectedReaspons()
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            DataTable dt = rejectReason;
            RootObjectRejectedReasons ri = new RootObjectRejectedReasons();
            RootRejectedReasons roi = new RootRejectedReasons();
            List<RejectedReasons> rr = new List<RejectedReasons>();
            int count = 0;
            if (dt.Rows.Count > 0)
            {
                count = dt.Rows.Count;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    RejectedReasons cls = new RejectedReasons
                    {
                        reason_description = dt.Rows[i]["description"].ToString()
                    };
                    rr.Add(cls);
                }
                roi.reason_descriptions = rr;
                ri.root = roi;
            }
            ri.count = count;
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            //logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/gsk-retailers/get")]
        [CacheOutput(ClientTimeSpan = 4000, ServerTimeSpan = 4000)]
        public HttpResponseMessage GetGskRetailerMappingUsingRetailerId(string retailer_id = "")
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            string query = string.Empty;
            if (!string.IsNullOrEmpty(retailer_id))
            {
                query = string.Format(@"{0} where Identifier_ID like '%{1}%'", retailerMappingQuery, retailer_id);
            }
            else
            {
                query = retailerMappingQuery;
            }

            DataTable dt = gi.GetDataMapping(query, connectionString);
            RootIdentifierRetailer ri = new RootIdentifierRetailer();
            RootObjectIdentifierRetailer roi = new RootObjectIdentifierRetailer();
            List<IdentifierRetailer> clslist = new List<IdentifierRetailer>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    IdentifierRetailer cls = new IdentifierRetailer
                    {
                        id = dt.Rows[i]["SerialNo"].ToString(),
                        gsk_retailer_code = dt.Rows[i]["GSK_Retailer_Code"].ToString(),
                        gsk_retailer_name = dt.Rows[i]["GSK_Retailer_Name"].ToString(),
                        stockiest_retailer_identifier = dt.Rows[i]["Identifier_ID"].ToString(),
                        stockiest_retailer_identifier_type = dt.Rows[i]["Type"].ToString()
                    };
                    clslist.Add(cls);
                }
                ri.retailers_mapping = clslist;
                roi.root = ri;
            }

            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            //logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        #endregion
    }
}