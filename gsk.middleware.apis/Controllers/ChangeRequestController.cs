﻿using gsk.middleware.apis.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class ChangeRequestController : ApiController
    {
        #region Variables
        private Logger logger;
        private ApiCall apiCall = ApiCall.GetInstance;
        private readonly string connectionString, purchaseUnderstandingQuery, invoiceImageHistoryQuery;
        private string iunid, iunPwd;
        private readonly string liveOrgTillId, webengageAccountId;
        private readonly string customerTrasnactionGetApi, changeReqeustAPI, v2Api, customerGetAPI;

        private readonly DataTable pmaster;
        static object productMasterTable = new object();
        const string productMasterCache = "productMasterCache";
        #endregion

        public ChangeRequestController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            GetMappings.Mappings();
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
            connectionString = string.Format("{0}&e;", connectionString);
            iunid = ConfigurationManager.AppSettings["IunId"];
            iunPwd = ConfigurationManager.AppSettings["IunPwd"];
            liveOrgTillId = ConfigurationManager.AppSettings["LiveOrgTillId"];
            changeReqeustAPI = ConfigurationManager.AppSettings["RequestAPI"];
            v2Api = ConfigurationManager.AppSettings["CRMCustomerV2API"];
            customerGetAPI = ConfigurationManager.AppSettings["CustomerGetAPI"];
            webengageAccountId = ConfigurationManager.AppSettings["WebEngageAccountId"];
            #endregion
            pmaster = GetMappings.pmaster;
        }


        [Route("v1.1/mobile/change")]
        public HttpResponseMessage GskImageUpload(Models.ChangeRequest.RootObject root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Models.ChangeResponse.RootObject ri = new Models.ChangeResponse.RootObject();
            string jn = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn.ToString());
            try
            {
                if (root != null)
                {
                    string json2 = JsonConvert.SerializeObject(root, Formatting.Indented);
                    logger.Debug("Request " + json2);

                    string d = apiCall.IntouchMakeRequest(changeReqeustAPI, json2, iunid, iunPwd, "", "");
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Models.ChangeResponse.RootObject));
                    using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d)))
                    {
                        ri = (Models.ChangeResponse.RootObject)deserializer.ReadObject(ms);

                        // ChangeWebEngageDetails(ri, root.root.request.old_value, root.root.request.new_value);
                        var thread = new Thread(
       () => ChangeWebEngageDetails(ri, root.root.request.old_value, root.root.request.new_value));
                        thread.Start();
                    }
                }
                else
                {
                    ri.response.status.code = 500;
                    ri.response.status.success = false;
                    ri.response.status.message = "All requests have failed due to errors";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                ri.response.status.code = 500;
                ri.response.status.success = false;
                ri.response.status.message = "All requests have failed due to errors";
            }
            string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
            logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        void ChangeWebEngageDetails(Models.ChangeResponse.RootObject root, string oldMobileNumber, string newMobileNumber)
        {
            if (root.response.status.code == 200)
            {
                string user_id = string.Empty;
                try
                {
                    var userIdGetApi = string.Format(@"{0}&mobile={1}&user_id=true", customerGetAPI, newMobileNumber);
                    string d = apiCall.IntouchMakeRequest(userIdGetApi, "", iunid, iunPwd, "", "");
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Models.GetUserId.RootObject));
                    using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d)))
                    {
                        Models.GetUserId.RootObject ri = (Models.GetUserId.RootObject)deserializer.ReadObject(ms);
                        foreach (var cust in ri.response.customers.customer)
                        {
                            user_id = cust.user_id;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }

                if (!string.IsNullOrEmpty(user_id))
                {
                    Models.ChangeWebEngageIdentifier.RootObject rootObject = new Models.ChangeWebEngageIdentifier.RootObject();
                    List<Models.ChangeWebEngageIdentifier.Add> adds = new List<Models.ChangeWebEngageIdentifier.Add>();
                    List<Models.ChangeWebEngageIdentifier.Remove> removes = new List<Models.ChangeWebEngageIdentifier.Remove>();
                    Models.ChangeWebEngageIdentifier.Add addMobile = new Models.ChangeWebEngageIdentifier.Add();
                    Models.ChangeWebEngageIdentifier.Add addCuid = new Models.ChangeWebEngageIdentifier.Add();
                    Models.ChangeWebEngageIdentifier.Remove removeMobile = new Models.ChangeWebEngageIdentifier.Remove();
                    Models.ChangeWebEngageIdentifier.Remove removeCuid = new Models.ChangeWebEngageIdentifier.Remove();
                    Models.V2ApiResponse.RootObject ri = new Models.V2ApiResponse.RootObject();
                   
                    addMobile.type = "mobile";
                    addMobile.value = newMobileNumber;
                    removeMobile.type = "mobile";
                    removeMobile.value = oldMobileNumber;

                    addCuid.type = "cuid";
                    addCuid.value = newMobileNumber;
                    removeCuid.type = "cuid";
                    removeCuid.value = oldMobileNumber;

                    adds.Add(addMobile);
                    adds.Add(addCuid);
                    removes.Add(removeMobile);
                    removes.Add(removeCuid);

                    rootObject.add = adds;
                    rootObject.remove = removes;

                    string json2 = JsonConvert.SerializeObject(rootObject, Formatting.Indented);
                    logger.Debug("Request " + json2);
                    var V2ChangeIdentiferAPI = string.Format(@"{0}{1}/changeIdentifier?source=WEB_ENGAGE&accountId={2}", v2Api, user_id,webengageAccountId);
                    string d = apiCall.IntouchMakeRequest(V2ChangeIdentiferAPI, json2, iunid, iunPwd, "", "");
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(Models.V2ApiResponse.RootObject));
                    using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d)))
                    {
                        ri = (Models.V2ApiResponse.RootObject)deserializer.ReadObject(ms);
                    }

                    if (!string.IsNullOrEmpty(ri.createdId.ToString()) && ri.createdId > 0)
                    {
                        List<Models.ChangeCommunicationChennal.Profile> profiles = new List<Models.ChangeCommunicationChennal.Profile>();
                        List<Models.ChangeCommunicationChennal.Identifier> identifiers = new List<Models.ChangeCommunicationChennal.Identifier>();
                        List<Models.ChangeCommunicationChennal.CommChannel> commChannels = new List<Models.ChangeCommunicationChennal.CommChannel>();
                        
                        Models.ChangeCommunicationChennal.RootObject rootObject1 = new Models.ChangeCommunicationChennal.RootObject();
                        Models.ChangeCommunicationChennal.Profile profile = new Models.ChangeCommunicationChennal.Profile();
                        Models.ChangeCommunicationChennal.Identifier identifier = new Models.ChangeCommunicationChennal.Identifier();
                        Models.ChangeCommunicationChennal.Meta meta = new Models.ChangeCommunicationChennal.Meta();
                        Models.ChangeCommunicationChennal.CommChannel commChannel = new Models.ChangeCommunicationChennal.CommChannel();

                        identifier.type = "mobile";
                        identifier.value = newMobileNumber;

                        identifiers.Add(identifier);

                        commChannel.type = "mobile";
                        commChannel.value = newMobileNumber;
                        commChannel.primary = "true";
                        commChannel.verified = "true";
                        
                        meta.residence = "true";
                        commChannel.meta = meta;

                        commChannels.Add(commChannel);
                        profile.commChannels = commChannels;

                        profile.source = "WEB_ENGAGE";
                        profile.accountId = webengageAccountId;

                        profiles.Add(profile);

                        rootObject1.profiles = profiles;

                        var V2CommunicateChenalChangeAPI = string.Format(@"{0}{1}?source=WEB_ENGAGE&accountId={2}", v2Api, user_id,webengageAccountId);

                        string json = JsonConvert.SerializeObject(rootObject1, Formatting.Indented);
                        logger.Debug("Request " + json);
                        string d1 = apiCall.IntouchMakeRequest(V2CommunicateChenalChangeAPI, json, iunid, iunPwd, "", "");
                        DataContractJsonSerializer deserializer1 = new DataContractJsonSerializer(typeof(Models.V2ApiResponse.RootObject));
                        using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d1)))
                        {
                            Models.V2ApiResponse.RootObject ri1 = (Models.V2ApiResponse.RootObject)deserializer1.ReadObject(ms);
                        }
                    }
                }

            }
        }
    }
}
