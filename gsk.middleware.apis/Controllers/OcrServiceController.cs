﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Runtime.Caching;

namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]

    public class OcrServiceController : ApiController
    {
        #region Variables
        private Logger logger;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private ApiCall apiCall = ApiCall.GetInstance;
        private string[] ocrIamgeStatus = { "OPEN", "CLOSED", "PROCESSING", "FAILED" };
        private readonly string connectionString;
        #endregion

        public OcrServiceController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
           connectionString = string.Format("{0}&e;", connectionString);
            #endregion
        }
        
        #region OCR Service
        [Route("v1.1/ocr-image-data/add")]
        public HttpResponseMessage OcrImageData(Models.Ocr.Data.RootObject root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();

            try
            {
                if (root != null)
                {
                    string json = JsonConvert.SerializeObject(root, Formatting.Indented);
                    logger.Debug(json);

                    foreach (Models.Ocr.Data.OcrImageInvoice trans in root.root.ocr_image_invoice)
                    {
                        if (string.IsNullOrEmpty(trans.image_id))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "Image id can't be null or empty";
                        }
                        else if (string.IsNullOrEmpty(trans.ocr_job_id))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "OCR Job id can't be null or empty";
                        }
                        else if (string.IsNullOrEmpty(trans.job_id_status))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "OCR Job Status can't be null or empty";
                        }
                        else if (string.IsNullOrEmpty(trans.image_update_time))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "OCR update time can't be null or empty";
                        }
                        else
                        {
                            bool flag = true;
                            try
                            {
                                DateTime d1 = Convert.ToDateTime(trans.image_update_time);
                            }
                            catch (Exception)
                            {
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "OCR update time is not correct format";
                            }

                            if (flag)
                            {
                                string jsonfilepath = @"C:/GSK/OcrData/";

                                if (!Directory.Exists(jsonfilepath))
                                {
                                    Directory.CreateDirectory(jsonfilepath);
                                }

                                if (Directory.Exists(jsonfilepath))
                                {
                                    {
                                        try
                                        {
                                            string json2 = JsonConvert.SerializeObject(trans.ocrData, Formatting.Indented);
                                            StreamWriter sw = new StreamWriter(string.Format(@"{2}\{0}_{1}.json", trans.image_id, trans.ocr_job_id, jsonfilepath));
                                            sw.WriteLine(json2);
                                            sw.Close();
                                            sw.Dispose();
                                            status.code = 200;
                                            status.success = true;
                                            status.message = "Job ID Status is saved suceessfully";

                                            using (MySqlConnection connection = new MySqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                MySqlCommand mycommand = new MySqlCommand("sp_image_ocr_id", connection)
                                                {
                                                    CommandType = CommandType.StoredProcedure
                                                };
                                                mycommand.Parameters.AddWithValue("@P_Image_id", trans.image_id != null ? trans.image_id : string.Empty);
                                                mycommand.Parameters.AddWithValue("@ocr_job_id", trans.ocr_job_id != null ? trans.ocr_job_id : string.Empty);
                                                mycommand.Parameters.AddWithValue("@job_id_status", trans.ocr_job_id != null ? trans.job_id_status : string.Empty);
                                                mycommand.Parameters.AddWithValue("@ocr_job_id_update_date", trans.image_update_time != null ? trans.image_update_time : Validation.IST().ToString("yyyy-MM-dd HH:mm:ss"));
                                                try
                                                {
                                                    int d = mycommand.ExecuteNonQuery();
                                                    if (d > 0)
                                                    {
                                                        status.code = 200;
                                                        status.success = true;
                                                        status.message = string.Format("Job ID \"{1}\" of invoice image \"{0}\" updated with \"{2}\" status successfully", trans.image_id, trans.ocr_job_id, trans.job_id_status);
                                                    }
                                                    else
                                                    {
                                                        status.code = 500;
                                                        status.success = false;
                                                        status.message = string.Format("Invoice Id \"{0}\" not exits", trans.image_id, trans.job_id_status);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    logger.Error(ex.ToString());

                                                    status.code = 500;
                                                    status.success = false;
                                                    status.message = "All requests have failed due to errors";
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error(ex.ToString());
                                            status.code = 500;
                                            status.success = false;
                                            status.message = "All requests have failed due to errors";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    logger.Error("Request body is empty");
                    status.code = 500;
                    status.success = false;
                    status.message = "All requests have failed due to errors";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.success = false;
                status.message = "All requests have failed due to errors";
            }
            response.status = status;
            rootResponse.response = response;
            string jn1 = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
            logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        // Not in Use
        [HttpPost]
        [Route("v1.1/ocr/job-id")]
        public HttpResponseMessage GskImageOcrJobIdUpload(Models.Ocr.RootObject root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string jn = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn.ToString());
            try
            {
                if (root != null)
                {
                    foreach (Models.Ocr.OcrImageInvoice trans in root.root.ocr_image_invoice)
                    {
                        if (string.IsNullOrEmpty(trans.image_id))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "Image id can't be null or empty";
                        }
                        else if (string.IsNullOrEmpty(trans.ocr_job_id))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "OCR Job id can't be null or empty";
                        }
                        else if (string.IsNullOrEmpty(trans.job_id_status))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "OCR Job Status can't be null or empty";
                        }
                        else if (string.IsNullOrEmpty(trans.image_update_time))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "OCR update time can't be null or empty";
                        }

                        else
                        {
                            bool flag = true;
                            try
                            {
                                DateTime d1 = Convert.ToDateTime(trans.image_update_time);
                            }
                            catch (Exception)
                            {
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "OCR update time is not correct format";
                            }
                            if (!ocrIamgeStatus.Contains(trans.job_id_status))
                            {
                                flag = false;
                                status.code = 500;
                                status.success = false;
                                status.message = "OCR update Status is Invaild. It should be OPEN / CLOSED / PROCESSING / FAILED.";
                            }

                            if (flag)
                            {
                                using (MySqlConnection connection = new MySqlConnection(connectionString))
                                {
                                    connection.Open();
                                    MySqlCommand mycommand = new MySqlCommand("sp_image_ocr_id", connection)
                                    {
                                        CommandType = CommandType.StoredProcedure
                                    };
                                    mycommand.Parameters.AddWithValue("@P_Image_id", trans.image_id != null ? trans.image_id : string.Empty);
                                    mycommand.Parameters.AddWithValue("@ocr_job_id", trans.ocr_job_id != null ? trans.ocr_job_id : string.Empty);
                                    mycommand.Parameters.AddWithValue("@job_id_status", trans.ocr_job_id != null ? trans.job_id_status : string.Empty);
                                    mycommand.Parameters.AddWithValue("@ocr_job_id_update_date", trans.image_update_time != null ? trans.image_update_time : Validation.IST().ToString("yyyy-MM-dd HH:mm:ss"));
                                    try
                                    {

                                        int d = mycommand.ExecuteNonQuery();
                                        if (d > 0)
                                        {
                                            status.code = 200;
                                            status.success = true;
                                            status.message = string.Format("invoice image \"{0}\" information updated with OCR ID -\"{1}\" successfully", trans.image_id, trans.ocr_job_id);
                                        }
                                        else
                                        {
                                            status.code = 500;
                                            status.success = false;
                                            status.message = string.Format("{0} {1}", trans.image_id, "invoice Id not exists");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex.ToString());

                                        status.code = 500;
                                        status.success = false;
                                        status.message = "All requests have failed due to errors";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    status.code = 500;
                    status.success = false;
                    status.message = "All requests have failed due to errors";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.success = false;
                status.message = "All requests have failed due to errors";
            }
            response.status = status;
            rootResponse.response = response;
            string jn1 = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
            logger.Debug(jn1);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        #endregion
    }
}