﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;
using System.Runtime.Caching;


namespace gsk.middleware.apis.Controllers
{
    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class UsersController : ApiController
    {
        #region Variables
        private Logger logger;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private readonly string connectionString;
        #endregion

        public UsersController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
           connectionString = string.Format("{0}&e;", connectionString);
            #endregion
        }

        #region HelpDesk User

        [Route("v1.1/executive-login/update")]
        public HttpResponseMessage ExecutiveLoginUpdate(RootObjectExecutiveLogins root)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string jn1 = JsonConvert.SerializeObject(root, Formatting.Indented);
            logger.Debug(jn1);
            string username = string.Empty;
            username = System.Web.HttpContext.Current.User.Identity.Name;
            ApiIdentity identity = (ApiIdentity)System.Web.HttpContext.Current.User.Identity;
            username = identity.User.UserName;
            try
            {
                if (root != null)
                {
                    foreach (ExecutiveLogins trans in root.root.logins)
                    {
                        if (string.IsNullOrEmpty(trans.empId) || string.IsNullOrEmpty(trans.password.ToString()))
                        {
                            status.code = 500;
                            status.success = false;
                            status.message = "All requests have failed due to errors";
                        }
                        else
                        {
                            bool flag = true;
                            if (flag)
                            {
                                bool checkPwd = Validation.IsMD5(trans.password.ToString());
                                if (checkPwd)
                                {
                                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                                    {
                                        connection.Open();
                                        MySqlCommand mycommand = new MySqlCommand("sp_executive_login_update", connection)
                                        {
                                            CommandType = CommandType.StoredProcedure
                                        };
                                        mycommand.Parameters.AddWithValue("@empId", trans.empId.ToString());
                                        mycommand.Parameters.AddWithValue("@loginId", username);
                                        mycommand.Parameters.AddWithValue("@password", trans.password.ToString());
                                        mycommand.Parameters.AddWithValue("@P_updated_time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                        try
                                        {

                                            int numberOfRecords = mycommand.ExecuteNonQuery();
                                            if (numberOfRecords > 0)
                                            {
                                                status.code = 200;
                                                status.success = true;
                                                status.message = "Password updated successfully";
                                            }
                                            else
                                            {
                                                status.code = 500;
                                                status.success = false;
                                                status.message = "no record updated";
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error(ex.ToString());

                                            status.code = 500;
                                            status.success = false;
                                            status.message = "All requests have failed due to errors";
                                        }
                                    }
                                }
                                else
                                {
                                    status.code = 500;
                                    status.success = false;
                                    status.message = "Password should be MD5 Encrypted";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                status.code = 500;
                status.success = false;
                status.message = "All requests have failed due to errors";
            }
            response.status = status;
            rootResponse.response = response;
            string jn = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
            logger.Debug(jn);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        [Route("v1.1/gsk-executive-login/get")]
        [CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage GetExecutiveLogisn()
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string username = string.Empty;
            string password = string.Empty;
            username = System.Web.HttpContext.Current.User.Identity.Name;
            ApiIdentity identity = (ApiIdentity)System.Web.HttpContext.Current.User.Identity;
            username = identity.User.UserName;
            password = identity.User.Password;

            string query = string.Empty;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                query = string.Format(@"{3} where   login_id = '{1}' and 	password = '{2}'", "", username, password, ConfigurationManager.AppSettings["ExecutiveLogin"]);
            }
            else
            {
                status.code = 401;
                status.message = "Please check your username/password or authentication token";
                status.success = false;
                response.status = status;
                rootResponse.response = response;
                return Request.CreateResponse(HttpStatusCode.OK, rootResponse);
            }

            DataTable dt = gi.GetDataMapping(query, connectionString);
            RootObjectExecutiveLogins ri = new RootObjectExecutiveLogins();
            RootExecutiveLogins roi = new RootExecutiveLogins();
            List<ExecutiveLogins> clslist = new List<ExecutiveLogins>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ExecutiveLogins cls = new ExecutiveLogins
                    {
                        name = dt.Rows[i]["Name"].ToString(),
                        empId = dt.Rows[i]["Emp_Id"].ToString(),
                        loginId = dt.Rows[i]["login_id"].ToString(),
                        newLogin = dt.Rows[i]["newLogin"].ToString(),
                        createdDate = dt.Rows[i]["Created_date"].ToString(),
                        lastModifiedDate = dt.Rows[i]["last_modified_date"].ToString()
                    };
                    clslist.Add(cls);
                }
                roi.logins = clslist;
                ri.root = roi;
                string jn1 = JsonConvert.SerializeObject(ri, Formatting.Indented);
                //logger.Debug(jn1);
                HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, ri);
                res1.Headers.Add("request_Id", request_Id);
                return res1;
            }
            else
            {
                status.code = 401;
                status.message = "Please check your username/password or authentication token";
                status.success = false;
                response.status = status;
                rootResponse.response = response;
                string jn1 = JsonConvert.SerializeObject(rootResponse, Formatting.Indented);
                //logger.Debug(jn1);
                HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rootResponse);
                res1.Headers.Add("request_Id", request_Id);
                return res1;
            }
        }
        #endregion
    }
}