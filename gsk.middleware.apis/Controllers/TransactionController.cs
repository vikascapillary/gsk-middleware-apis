﻿using gsk.middleware.apis.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.OutputCache.V2;
using System.Runtime.Caching;

namespace gsk.middleware.apis.Controllers
{

    [BasicAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]

    public class TransactionController : ApiController
    {
        #region Variables
        private Logger logger;
        private MemoryCache cache = new MemoryCache("CacheKey");
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        private ApiCall apiCall = ApiCall.GetInstance;
        string smsSendApi, smsTemplate, getBillNumberApi;
        private MultiPageInvoiceHandling multiPageInvoiceHandling = MultiPageInvoiceHandling.GetInstance;
        private string[] ocrIamgeStatus = { "OPEN", "CLOSED", "PROCESSING", "FAILED" };
        private readonly string connectionString, skuMappingQuery, stockiestQuery, skuMappingItemDescQuery, retailerMappingQuery, purchaseUnderstandingQuery, invoiceImageHistoryQuery;
        string invoiceImageQuery;
        private readonly string intouchTrasnactionAddApi;
        private string iunid;
        private readonly string iunPwd;
        private readonly string getS3ApiUrl;
        private readonly string auth;
        private readonly string productGetApi, merchantguid, aesKey, saleswalletid, mId;
        private readonly string payTmAmountAddApi, payTmTxnCheckApi, retryCount, sleepTime;
        private string payTmFailureCode;
        private string txnFailureCode;
        private readonly string retailerImageQuery;
        private readonly string appendingNumberTill, liveOrgTillId, customerGetApi;
        private readonly string brandName, customerTrasnactionGetApi, pointRedeemApi;
        private readonly string pointsReddem, pointsIsRedeem;
        private readonly DataTable mfg, pmapping, pmaster;
        string getBillNumber;
        static object mfgTable = new object();
        const string mfgCache = "mfgCache";
        static object productMappingTable = new object();
        const string productMappingCache = "productMappingCache";
        static object productMasterTable = new object();
        const string productMasterCache = "productMasterCache";
        private readonly DataTable rejectReason;
        static object rejectReasonTable = new object();
        const string rejectReasonCache = "rejectReasonCache";

        #endregion
        public TransactionController()
        {
            logger = LogManager.GetCurrentClassLogger();
            #region Reading Config
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
         connectionString = string.Format("{0}&e;", connectionString);
            skuMappingQuery = ConfigurationManager.AppSettings["SkuMappingQuery"];
            stockiestQuery = ConfigurationManager.AppSettings["StockiestQuery"];
            skuMappingItemDescQuery = ConfigurationManager.AppSettings["SkuMappingItemDescQuery"];
            retailerMappingQuery = ConfigurationManager.AppSettings["RetailerMappingQuery"];
            purchaseUnderstandingQuery = ConfigurationManager.AppSettings["PurchaseUnderstandingQuery"];
            invoiceImageQuery = ConfigurationManager.AppSettings["InvoiceImageQuery"];
            invoiceImageHistoryQuery = ConfigurationManager.AppSettings["InvoiceImageHistoryQuery"];
            retailerImageQuery = ConfigurationManager.AppSettings["RetailerImageQuery"];
            productGetApi = ConfigurationManager.AppSettings["IntouchProductGetAPI"];
            getBillNumberApi = ConfigurationManager.AppSettings["IntouchBillNumberGetAPI"];
            smsSendApi = ConfigurationManager.AppSettings["SmsSendApi"];
            smsTemplate = ConfigurationManager.AppSettings["SmsTemplate"];
            intouchTrasnactionAddApi = ConfigurationManager.AppSettings["IntouchTransactionAddAPI"];
            getBillNumber = ConfigurationManager.AppSettings["GetBillNumber"];
            iunid = ConfigurationManager.AppSettings["IunId"];
            iunPwd = ConfigurationManager.AppSettings["IunPwd"];

            getS3ApiUrl = ConfigurationManager.AppSettings["GetS3ApiURL"];
            auth = ConfigurationManager.AppSettings["GetS3ApiUrlAuth"];

            merchantguid = ConfigurationManager.AppSettings["Merchantguid"];
            aesKey = ConfigurationManager.AppSettings["AesKey"];
            saleswalletid = ConfigurationManager.AppSettings["Saleswalletid"];
            mId = ConfigurationManager.AppSettings["MId"];
            payTmAmountAddApi = ConfigurationManager.AppSettings["PayTmAddAmountApi"];
            payTmTxnCheckApi = ConfigurationManager.AppSettings["PayTmCheckTxnApi"];
            retryCount = ConfigurationManager.AppSettings["RetryCount"];
            sleepTime = ConfigurationManager.AppSettings["SleepTime"];

            payTmFailureCode = ConfigurationManager.AppSettings["PayTMFailureCode"];
            txnFailureCode = ConfigurationManager.AppSettings["TxnFailureCodes"];
            brandName = ConfigurationManager.AppSettings["BrandName"];

            appendingNumberTill = ConfigurationManager.AppSettings["AppendingNumberInTillId"];
            liveOrgTillId = ConfigurationManager.AppSettings["LiveOrgTillId"];
            customerGetApi = ConfigurationManager.AppSettings["CustomerGetAPI"];
            customerTrasnactionGetApi = ConfigurationManager.AppSettings["IntouchTransactionGetAPI"];
            pointRedeemApi = ConfigurationManager.AppSettings["IntouchPointRedeemAPI"];
            pointsReddem = ConfigurationManager.AppSettings["PointsRedeem"];
            pointsIsRedeem = ConfigurationManager.AppSettings["IntouchPointIsRedeemAPI"];

            #endregion
        }

       
        [HttpGet]
        [Route("v1.1/sms")]
        public HttpResponseMessage SendSmsForRejectInvoice(string customer_id, string rejectReason, string imageId)
        {
            var mobile = string.Empty;
            string customerGetAPI = string.Format(@"{1}&external_id={0}", customer_id, customerGetApi);
            string custResponse = apiCall.IntouchMakeRequest(customerGetAPI, "", liveOrgTillId, iunPwd, "", "");
            DataContractJsonSerializer custdes = new DataContractJsonSerializer(typeof(RootObjectCustomer));
            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(custResponse)))
            {
                RootObjectCustomer rcust = (RootObjectCustomer)custdes.ReadObject(ms);
                foreach (Customer cust in rcust.response.customers.customer)
                {
                    mobile = cust.mobile;
                }
            }
            Sm sms = new Sm();
            List<Sm> smslist = new List<Sm>();
            RootObjectSms rootObjectSms = new RootObjectSms();
            RootSms rootSms = new RootSms();
            sms.to = mobile;
            sms.scheduled_time = Validation.IST().ToString("yyyy-MM-dd HH:mm:ss");
            string message = $@"{smsTemplate}";
            sms.body = string.Format(@"{0} {3}", message, smsTemplate, Environment.NewLine, rejectReason, imageId);
            smslist.Add(sms);
            rootSms.sms = smslist;
            rootObjectSms.root = rootSms;
            string json2 = JsonConvert.SerializeObject(rootObjectSms, Formatting.Indented);

            string smsSendResponse = apiCall.IntouchMakeRequest(smsSendApi, json2, iunid, iunPwd, "", "");
            return null;
        }

        #region Misc
        [HttpGet]
        [Route("v1.1/customer/get")]
        [CacheOutput(ClientTimeSpan = 60, ServerTimeSpan = 60)]
        public HttpResponseMessage GetCustomer(string mob, string external_id)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);

            Models.ResponseApp.Root root = new Models.ResponseApp.Root();
            Models.ResponseApp.RootObject returnObejct = new Models.ResponseApp.RootObject();
            Models.ResponseApp.RedemptionsStatus status = new Models.ResponseApp.RedemptionsStatus();
            Models.ResponseApp.PromotionPointsStatus pstatus = new Models.ResponseApp.PromotionPointsStatus();
            List<Models.ResponseApp.RedeemPoint> redeemPoints = new List<Models.ResponseApp.RedeemPoint>();
            List<List<Models.ResponseApp.PromotionPoints>> promotionPoints = new List<List<Models.ResponseApp.PromotionPoints>>();
            List<Models.ResponseApp.PromotionPoints> promotions = new List<Models.ResponseApp.PromotionPoints>();
            List<List<Models.ResponseApp.ExpiredPoints>> expiredPoints = new List<List<Models.ResponseApp.ExpiredPoints>>();
            List<Models.ResponseApp.ExpiredPoints> expireds = new List<Models.ResponseApp.ExpiredPoints>();

            Models.customergetResponse.RootObject customerRes = GetByExternalId(mob, external_id);

            pstatus.success = customerRes.response.status.success;
            pstatus.message = customerRes.response.status.message;
            pstatus.code = customerRes.response.status.code;
            returnObejct.promotion_points_status = pstatus;
            returnObejct.status = pstatus;
            if (pstatus.code == 200)
            {
                try
                {
                    Models.RedemptionResponse.RootObject redRes = GetRedemption(mob, external_id);
                    if (redRes.response != null)
                    {
                        status.code = redRes.response.status.code;
                        status.message = redRes.response.status.message;
                        status.success = redRes.response.status.success;
                        returnObejct.redemptions_status = status;
                        foreach (Models.RedemptionResponse.Point points in redRes.response.customer.redemptions.points.point)
                        {
                            Models.ResponseApp.RedeemedStore rs = new Models.ResponseApp.RedeemedStore();
                            Models.ResponseApp.RedeemedTill rt = new Models.ResponseApp.RedeemedTill();
                            Models.ResponseApp.RedeemPoint redeemPoint = new Models.ResponseApp.RedeemPoint
                            {
                                id = points.id,
                                notes = points.notes,
                                points_redeemed = points.points_redeemed,
                                program_id = points.program_id,
                                redeemed_at = points.redeemed_at
                            };
                            rs.code = points.redeemed_store.code;
                            rs.name = points.redeemed_store.name;
                            rt.name = points.redeemed_till.name;
                            rt.code = points.redeemed_till.code;
                            redeemPoint.redeemed_store = rs;
                            redeemPoint.redeemed_till = rt;
                            redeemPoint.redeemed_time = points.redeemed_time;
                            redeemPoint.transaction_number = points.transaction_number;
                            redeemPoint.validation_code = points.validation_code;
                            redeemPoints.Add(redeemPoint);
                        }
                        returnObejct.expired_points = expiredPoints;
                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }

            List<string> firstName = new List<string>();
            List<string> lastname = new List<string>();

            #region Custom Fields
            List<Models.ResponseApp.CustomField> cfList = new List<Models.ResponseApp.CustomField>();
            Models.ResponseApp.CustomField cfp = new Models.ResponseApp.CustomField();
            List<Models.ResponseApp.Field> cfl = new List<Models.ResponseApp.Field>();

            List<Models.ResponseApp.ExtendedField> efList = new List<Models.ResponseApp.ExtendedField>();
            Models.ResponseApp.ExtendedField efp = new Models.ResponseApp.ExtendedField();
            List<Models.ResponseApp.Field2> efl = new List<Models.ResponseApp.Field2>();

            List<Models.ResponseApp.Item> pil = new List<Models.ResponseApp.Item>();
            if (customerRes.response.customers.customer != null)
            {
                foreach (Models.customergetResponse.Customer cust in customerRes.response.customers.customer)
                {
                    returnObejct.mobile = cust.mobile;
                    firstName.Add(cust.firstname);
                    lastname.Add(cust.lastname);
                    returnObejct.external_id = cust.external_id;
                    returnObejct.firstname = firstName;
                    returnObejct.lastname = lastname;
                    if (cust.custom_fields != null)
                    {
                        foreach (Models.customergetResponse.Field cf in cust.custom_fields.field)
                        {
                            Models.ResponseApp.Field cfa = new Models.ResponseApp.Field
                            {
                                name = cf.name,
                                value = cf.value
                            };
                            cfl.Add(cfa);
                        }
                        cfp.field = cfl;
                    }
                    if (cust.extended_fields != null)
                    {
                        foreach (Models.customergetResponse.Field2 cf in cust.extended_fields.field)
                        {
                            Models.ResponseApp.Field2 cfa = new Models.ResponseApp.Field2
                            {
                                name = cf.name,
                                value = cf.value
                            };
                            efl.Add(cfa);
                        }
                        efp.field = efl;
                    }

                    if (cust.promotion_points != null)
                    {
                        Models.ResponseApp.PromotionPoints pp = new Models.ResponseApp.PromotionPoints();
                        Models.ResponseApp.Customer2 pc = new Models.ResponseApp.Customer2();
                        Models.ResponseApp.Lineitems pl = new Models.ResponseApp.Lineitems();
                        List<Models.ResponseApp.Item2> pll = new List<Models.ResponseApp.Item2>();
                        List<List<Models.ResponseApp.Item2>> plll = new List<List<Models.ResponseApp.Item2>>();
                        if (cust.promotion_points.customer.item.Count() > 0)
                        {
                            foreach (Models.customergetResponse.Item2 item in cust.promotion_points.customer.item)
                            {
                                Models.ResponseApp.Item2 pi = new Models.ResponseApp.Item2();
                                Models.ResponseApp.IssuedAt pia = new Models.ResponseApp.IssuedAt();
                                Models.ResponseApp.PromotionSource pps = new Models.ResponseApp.PromotionSource();
                                pi.expiry_date = item.expiry_date;
                                pia.code = item.issued_at.code;
                                pia.name = item.issued_at.name;
                                pi.issued_on = item.issued_on;
                                pi.points = item.points;
                                pi.program_id = item.program_id;
                                pps.source_id = item.promotion_source.source_id;
                                pps.source_type = item.promotion_source.source_type;
                                pi.promotion_source = pps;
                                pi.issued_at = pia;
                                pll.Add(pi);
                            }
                        }
                        pc.item = pll;
                        plll.Add(pll);
                        if (pll.Count() > 0)
                        {
                            returnObejct.promotion_points = plll;
                        }
                    }

                    if (cust.expired_points != null)
                    {
                        Models.ResponseApp.ExpiredPoints ep = new Models.ResponseApp.ExpiredPoints();
                        List<Models.ResponseApp.Item> ei = new List<Models.ResponseApp.Item>();
                        foreach (Models.customergetResponse.Item item in cust.expired_points.item)
                        {
                            Models.ResponseApp.Item i = new Models.ResponseApp.Item
                            {
                                expired_on = item.expired_on,
                                points = item.points,
                                program_id = item.program_id
                            };
                            ei.Add(i);
                        }
                        ep.item = ei;
                        expireds.Add(ep);
                        expiredPoints.Add(expireds);
                    }
                }
            }
            if (cfp.field.Count() > 0)
            {
                cfList.Add(cfp);
            }
            if (efp.field.Count() > 0)
            {
                efList.Add(efp);
            }
            if (cfList.Count() > 0)
            {
                returnObejct.custom_fields = cfList;
            }
            if (efList.Count() > 0)
            {
                returnObejct.extended_fields = efList;
            }
            if (redeemPoints.Count() > 0)
            {
                returnObejct.redeem_point = redeemPoints;
            }
            root.root = returnObejct;
            #endregion

            string jn1 = JsonConvert.SerializeObject(returnObejct, Formatting.Indented);
            //logger.Debug(jn1);

            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, returnObejct);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }

        public Models.RedemptionResponse.RootObject GetRedemption(string mob, string external_id)
        {
            Models.RedemptionResponse.RootObject returnObj = new Models.RedemptionResponse.RootObject();
            try
            {
                string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
                logger.Debug("request_Id " + request_Id);
                int statusCode = 200;
                string redemptionGetApi = string.Format(@"https://apac2.intouch.capillarytech.com/v1.1/customer/redemptions?format=json&external_id={1}&points_limit=200", mob, external_id);
                string custResponse = apiCall.IntouchMakeRequest(redemptionGetApi, "", liveOrgTillId, iunPwd, "", "");
                //logger.Debug(custResponse);
                DataContractJsonSerializer custdes = new DataContractJsonSerializer(typeof(Models.RedemptionResponse.RootObject));
                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(custResponse)))
                {
                    try
                    {
                        Models.RedemptionResponse.RootObject rcust = (Models.RedemptionResponse.RootObject)custdes.ReadObject(ms);
                        statusCode = Convert.ToInt32(rcust.response.status.code);
                        if (mob != rcust.response.customer.mobile)
                        {
                            rcust.response.customer.redemptions.coupons = null;
                            rcust.response.customer.redemptions.points = null;
                        }
                        returnObj = rcust;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                    }
                }

                string jn1 = JsonConvert.SerializeObject(returnObj, Formatting.Indented);
                //logger.Debug(jn1);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return returnObj;
        }

        public Models.customergetResponse.RootObject GetByExternalId(string cust_mobile, string external_id)
        {
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);

            Models.customergetResponse.RootObject returnObj = null;
            #region Get Customer Using External Id
            int extItemStatusCode = 200;
            string customerGetAPI = string.Format(@"{1}&external_id={0}&expired_points=true&promotion_points=true", external_id, customerGetApi);
            string custResponse = apiCall.IntouchMakeRequest(customerGetAPI, "", liveOrgTillId, iunPwd, "", "");
            logger.Debug(custResponse);
            DataContractJsonSerializer custdes = new DataContractJsonSerializer(typeof(Models.customergetResponse.RootObject));
            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(custResponse)))
            {
                Models.customergetResponse.RootObject rcust = (Models.customergetResponse.RootObject)custdes.ReadObject(ms);
                extItemStatusCode = Convert.ToInt32(rcust.response.status.code);

                foreach (Models.customergetResponse.Customer cust in rcust.response.customers.customer)
                {
                    if (cust_mobile != cust.mobile)
                    {
                        if (!string.IsNullOrEmpty(cust.mobile))
                        {
                            cust.mobile = "919999999999";
                            rcust.mob = "yes";
                        }
                        else
                        {
                            cust.mobile = "";
                            rcust.mob = "no";
                        }
                        cust.firstname = "";
                        cust.lastname = "";
                        cust.email = "";
                        cust.lifetime_points = 0;
                        cust.lifetime_points = 0;
                        cust.loyalty_points = 0;
                        cust.current_slab = "";
                        cust.type = "";
                        cust.source = "";
                        cust.registered_by = "";
                        cust.registered_store = null;
                        cust.registered_till = null;
                        cust.fraud_details = null;
                        cust.expired_points = null;
                        cust.custom_fields = null;
                        cust.extended_fields = null;
                        cust.transactions = null;
                        cust.coupons = null;

                    }
                    else
                    {
                        rcust.mob = "matched";
                    }
                }
                returnObj = rcust;
            }
            #endregion

            string jn1 = JsonConvert.SerializeObject(returnObj, Formatting.Indented);
            //logger.Debug(jn1);
            return returnObj;
        }

        [HttpPost]
        [Route("v1.1/point-redeem/check")]
        public HttpResponseMessage GetPointRedeemStatus(PointsRootObject root)
        {
            System.Threading.Thread.Sleep(3000);
            RedeemedRootObject rro = new RedeemedRootObject();
            string request_Id = Request.Headers.GetValues("Request-Id").FirstOrDefault();
            logger.Debug("request_Id " + request_Id);
            if (root != null)
            {
                List<PointsRedeem> reddemd = root.root.redeem;
                foreach (PointsRedeem dr in reddemd)
                {
                    string points = dr.points_redeemed;
                    string mobile = dr.customer.mobile;
                    if (!string.IsNullOrEmpty(mobile))
                    {
                        if (mobile.Length == 12)
                        {
                            mobile = mobile.Substring(2, 10);
                            logger.Debug(mobile);
                        }
                        string getPointRedeemResponse = string.Format(@"SELECT * FROM  `PayTm_Transaction` where mobile_no='{0}' and points='{1}' and PointRedeemResponse!='' order by serial_no desc limit 1", mobile, points);
                        logger.Debug(getPointRedeemResponse);
                        DataTable dt = gi.GetDataMapping(getPointRedeemResponse, connectionString);
                        if (dt.Rows.Count == 1)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string res = dt.Rows[i]["PointRedeemResponse"].ToString();

                                if (!string.IsNullOrEmpty(res))
                                {
                                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RedeemedRootObject));
                                    using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(res)))
                                    {
                                        rro = (RedeemedRootObject)deserializer.ReadObject(ms);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            string jn = JsonConvert.SerializeObject(rro, Formatting.Indented);
            logger.Debug(jn);
            HttpResponseMessage res1 = Request.CreateResponse(HttpStatusCode.OK, rro);
            res1.Headers.Add("request_Id", request_Id);
            return res1;
        }
        #endregion
    }
}