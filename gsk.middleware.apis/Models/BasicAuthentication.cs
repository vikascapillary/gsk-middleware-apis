﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading;
using System.Security.Principal;
using System.Configuration;
using NLog;
using System.Web.Http;

namespace gsk.middleware.apis.Models
{
    public class BasicAuthentication : ActionFilterAttribute
    {
        GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        string connectionString;
        private string requestId { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            requestId = Guid.NewGuid().ToString("N");
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
          connectionString = string.Format("{0}&e;", connectionString);
            var ctx = actionContext.Request.Properties["MS_HttpContext"] as HttpContextWrapper;
            if (ctx != null)
            {
                var ip = ctx.Request.UserHostAddress;
                logger.Debug(string.Format(@"Client IP address: {0}{1}", ip, Environment.NewLine));
            }
            logger.Debug(actionContext.Request.RequestUri);
            actionContext.Request.Headers.Add("Request-Id", requestId);
            #region Local Variables
            Status status = new Status();
            AddResponse response = new AddResponse();
            RootAddResponse rootResponse = new RootAddResponse();
            string filename = string.Empty;
            #endregion

            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                #region Failure Response

                status.success = false;
                status.message = "Please check your username/password or authentication token";
                status.code = 401;
                response.status = status;
                rootResponse.response = response;
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, rootResponse);

                #endregion
            }

            else
            {
                string authenicationToken = actionContext.Request.Headers.Authorization.Parameter;
                string decodedAutheicationToke = Encoding.UTF8.GetString(Convert.FromBase64String(authenicationToken));
                string[] usernamePassword = decodedAutheicationToke.Split(':');
                string username = usernamePassword[0];
                string password = usernamePassword[1];
                User user = new User() { UserName = username, Password = password };
                var query = string.Empty;
                var f = true;
                logger.Debug(username);
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {

                    query = string.Format(@"SELECT * FROM  `ExecutiveLogin` where   login_id = '{0}' and 	password = '{1}'", username, password);
                }
                var dt = gi.GetDataMapping(query, connectionString);
                if (dt.Rows.Count>0)
                {
                    HttpContext.Current.User = new GenericPrincipal(new ApiIdentity(user), new string[] { });
                    base.OnActionExecuting(actionContext);
                }
                else
                {
                    #region Failure Response
                    
                    status.success = false;
                    status.message = "Please check your username/password or authentication token";
                    status.code = 401;
                    response.status = status;
                    rootResponse.response = response;
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, rootResponse);
                    
                    #endregion
                }
            }
        }
    }
}