﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis
{
    public class PointsCustomer
    {
        public string mobile { get; set; }
    }

    public class PointsRedeem
    {
        public string points_redeemed { get; set; }
        public string transaction_number { get; set; }
        public PointsCustomer customer { get; set; }
        public string notes { get; set; }
        public string validation_code { get; set; }
        public string redemption_time { get; set; }
    }

    public class PointsRoot
    {
        public List<PointsRedeem> redeem { get; set; }
    }

    public class PointsRootObject
    {
        public PointsRoot root { get; set; }
    }

    public class RedeemedStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class RedeemedEffect
    {
        public string case_value { get; set; }
        public int num_points { get; set; }
        public int currency_value { get; set; }
        public string validation_code { get; set; }
        public string points_redemption_summary_id { get; set; }
        public string redeemed_on_bill_number { get; set; }
        public int redeemed_on_bill_id { get; set; }
        public string type { get; set; }
    }

    public class RedeemedSideEffects
    {
        public List<RedeemedEffect> effect { get; set; }
    }

    public class RedeemedItemStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class RedeemedPoints
    {
        public string mobile { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public string user_id { get; set; }
        public string points_redeemed { get; set; }
        public int redeemed_value { get; set; }
        public int redeemed_local_value { get; set; }
        public int balance { get; set; }
        public RedeemedSideEffects side_effects { get; set; }
        public RedeemedItemStatus item_status { get; set; }
    }

    public class RedeemedResponses
    {
        public RedeemedPoints points { get; set; }
    }

    public class RedeemedResponse
    {
        public RedeemedStatus status { get; set; }
        public RedeemedResponses responses { get; set; }
    }

    public class RedeemedRootObject
    {
        public RedeemedResponse response { get; set; }
    }
}