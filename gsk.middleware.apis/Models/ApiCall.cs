﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using NLog;

namespace gsk.middleware.apis.Models
{
    public sealed class ApiCall
    {
        Logger logger;
        string connectionString, webengageAccountId;
        private static int coutner = 0;

        public static ApiCall GetInstance { get; } = new ApiCall();
        private ApiCall()
        {
            coutner++;
            logger = LogManager.GetCurrentClassLogger();
            webengageAccountId = ConfigurationManager.AppSettings["WebEngageAccountId"];
            logger.Debug("coutner "+coutner);
        }

        public  string MakeRequest(string api, string baseString)
        {
            var response = "";
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(api);
                httpWebRequest.Timeout = 3000000;
                httpWebRequest.KeepAlive = false;
                httpWebRequest.PreAuthenticate = true;
                httpWebRequest.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, string.Format("{0}", baseString));
                httpWebRequest.MediaType = "text/xml;charset=\"utf-8\"";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.UserAgent = "GLAXOSMITHKLINE";
                ServicePointManager.Expect100Continue = false;

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.Headers != null && !string.IsNullOrEmpty(Convert.ToString(httpWebResponse.Headers["X-Cap-RequestID"])))
                {

                }
                Stream stream = httpWebResponse.GetResponseStream();
                if (httpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    stream = (Stream)new GZipStream(stream, CompressionMode.Decompress);
                else if (httpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    stream = (Stream)new DeflateStream(stream, CompressionMode.Decompress);
                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8);

                response = streamReader.ReadToEnd();
            }
            catch (Exception)
            {

            }
            return response;
        }

        public string IntouchMakeRequest(string api, string postParameters, string capId, string capPwd, string billingTime, string imageid)
        {
        Start:
            bool success = false;
            logger.Debug(api);
            var requestId = string.Empty;
            string response = string.Empty;
            string message = string.Empty;
            string code = string.Empty;
            var mobile = string.Empty;
            var fName = string.Empty;
            var lName = string.Empty;
            var email = string.Empty;
            var apiResonseTime = string.Empty;
            var apiRequestTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            logger.Debug(capId);
            MD5CryptoServiceProvider cryptoServiceProvider = new MD5CryptoServiceProvider();
            string userName = capId;
            byte[] bytes1 = Encoding.ASCII.GetBytes(capPwd);
            byte[] hash = cryptoServiceProvider.ComputeHash(bytes1);
            StringBuilder stringBuilder = new StringBuilder(hash.Length * 2);
            foreach (byte num in hash)
                stringBuilder.AppendFormat("{0:x2}", num);
            string password = stringBuilder.ToString();
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(api);
                WebRequest.DefaultWebProxy = (IWebProxy)null;
                NetworkCredential networkCredential = new NetworkCredential(userName, password);

                httpWebRequest.Credentials = (ICredentials)networkCredential;
                httpWebRequest.Timeout = 3000000;
                httpWebRequest.KeepAlive = false;
                httpWebRequest.PreAuthenticate = true;
                httpWebRequest.Headers.Add(HttpRequestHeader.AcceptCharset, "utf-8");
                httpWebRequest.MediaType = "text/xml;charset=\"utf-8\"";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                if (api.Contains(@"points/"))
                {
                    httpWebRequest.Headers.Add("X-CAP-CLIENT-SIGNATURE", "true");
                }
                httpWebRequest.UserAgent = "Capillary WebClient";
                ServicePointManager.Expect100Continue = false;
                if (!string.IsNullOrEmpty(postParameters))
                {
                    logger.Debug(postParameters);
                    httpWebRequest.Method = "POST";
                    if (postParameters.Contains(@"commChannels") & api.Contains(webengageAccountId))
                    {
                        httpWebRequest.Method = "PUT";
                    }

                    byte[] bytes2 = Encoding.UTF8.GetBytes(postParameters);
                    httpWebRequest.ContentLength = (long)bytes2.Length;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    try
                    {
                        requestStream.Write(bytes2, 0, bytes2.Length);
                        requestStream.Close();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                        response = ex.Message.ToString();
                    }
                }
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpWebResponse.Headers != null && !string.IsNullOrEmpty(Convert.ToString(httpWebResponse.Headers["X-Cap-RequestID"])))
                {
                    requestId = Convert.ToString(httpWebResponse.Headers["X-Cap-RequestID"]);
                }
                Stream stream = httpWebResponse.GetResponseStream();
                if (httpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    stream = (Stream)new GZipStream(stream, CompressionMode.Decompress);
                else if (httpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    stream = (Stream)new DeflateStream(stream, CompressionMode.Decompress);
                StreamReader streamReader = new StreamReader(stream, Encoding.UTF8);
                response = streamReader.ReadToEnd();
                logger.Debug(response);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                response = ex.Message.ToString();
            }
            finally
            {
                if (response.Contains("response") && api.Contains(@"transaction/add"))
                {
                    ParseResponse(response, requestId, postParameters, billingTime,imageid,capId);
                }
            }
            return response;
        }

        DateTime IST()
        {
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo istZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime istTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, istZone);
            return istTime;
        }

        void ParseResponse(string jsonString, string requestId, string request, string billingTime, string imageid, string tillId)
        {
            try
            {
                connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
               connectionString = string.Format("{0}&e;", connectionString);
                logger.Debug(connectionString);
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RootObjectAddResponse));
                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(jsonString)))
                {
                    var response = (RootObjectAddResponse)deserializer.ReadObject(ms);
                    foreach (var trans in response.response.transactions.transaction)
                    {
                        using (MySqlConnection connection = new MySqlConnection(connectionString))
                        {
                            MySqlCommand mycommand = new MySqlCommand("sp_api_response", connection);
                            mycommand.CommandType = CommandType.StoredProcedure;
                            mycommand.Parameters.AddWithValue("@Bill_Number", trans.number.ToString());
                            mycommand.Parameters.AddWithValue("@Billing_DateTime", billingTime);
                            mycommand.Parameters.AddWithValue("@API_Response", jsonString);
                            mycommand.Parameters.AddWithValue("@API_Request", request);
                            mycommand.Parameters.AddWithValue("@Request_Id", requestId);
                            mycommand.Parameters.AddWithValue("@Retailer_Code", trans.customer.external_id);
                            mycommand.Parameters.AddWithValue("@Retailer_Name", "");
                            mycommand.Parameters.AddWithValue("@Stockiest_Code", tillId);
                            mycommand.Parameters.AddWithValue("@Stockiest_Name", "");
                            mycommand.Parameters.AddWithValue("@image_id", imageid);
                            mycommand.Parameters.AddWithValue("@API_Code", trans.item_status.code);
                            mycommand.Parameters.AddWithValue("@API_Message", trans.item_status.message);
                            mycommand.Parameters.AddWithValue("@Notes", "Retailer Interface Transaction");
                            mycommand.Parameters.AddWithValue("@APICallTime", IST().ToString("yyyy-MM-dd HH:mm:ss"));
                            connection.Open();
                            logger.Debug(connectionString);
                            mycommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public string PayTm(string json2, string url, string checksum, string merchantguid)
        {
            string responseData = string.Empty;
            try
            {
                logger.Debug(url);
                logger.Debug(json2);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "POST";
                webRequest.Accept = "application/json";
                webRequest.ContentType = "application/json";
                webRequest.Headers.Add("mid", merchantguid);
                webRequest.Headers.Add("checksumhash", checksum);
                webRequest.ContentLength = json2.ToString().Length;
                try
                {
                    using (StreamWriter requestWriter2 = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        requestWriter2.Write(json2);
                    }

                    using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                    {
                        responseData = responseReader.ReadToEnd();
                        logger.Debug(responseData);
                    }
                }
                catch (WebException web)
                {
                    logger.Error(web.ToString());
                    HttpWebResponse res = web.Response as HttpWebResponse;
                    Stream s = res.GetResponseStream();
                    string message;
                    using (StreamReader sr = new StreamReader(s))
                    {
                        message = sr.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return responseData;
        }
    }
}