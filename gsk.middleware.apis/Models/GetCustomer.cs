﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class GetCustomer
    {
    }

    public class GetCustomerField
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class GetCustomerCustomField
    {
        public List<GetCustomerField> field { get; set; }
    }

    public class GetCustomerField2
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class GetCustomerExtendedField
    {
        public List<GetCustomerField2> field { get; set; }
    }

    public class GetCustomerPromotionPointsStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public string total { get; set; }
        public string success_count { get; set; }
    }

    public class GetCustomerRedeemedStore
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetCustomerRedeemedTill
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class GetCustomerRedeemPoint
    {
        public int id { get; set; }
        public int program_id { get; set; }
        public int points_redeemed { get; set; }
        public string transaction_number { get; set; }
        public string validation_code { get; set; }
        public string redeemed_time { get; set; }
        public string redeemed_at { get; set; }
        public string notes { get; set; }
        public GetCustomerRedeemedStore redeemed_store { get; set; }
        public GetCustomerRedeemedTill redeemed_till { get; set; }
    }

    public class GetCustomerRedemptionsStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class GetCustomerRootObject
    {
        public string mobile { get; set; }
        public List<string> firstname { get; set; }
        public string external_id { get; set; }
        public List<GetCustomerCustomField> custom_fields { get; set; }
        public List<GetCustomerExtendedField> extended_fields { get; set; }
        public GetCustomerPromotionPointsStatus promotion_points_status { get; set; }
        public List<GetCustomerRedeemPoint> redeem_point { get; set; }
        public GetCustomerRedemptionsStatus redemptions_status { get; set; }
        public List<string> lastname { get; set; }
    }
}

