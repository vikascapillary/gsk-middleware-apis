﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class OfferAccepted
    {
        public string item_code { get; set; }
        public string item_description { get; set; }
        public string  qty { get; set; }
        public string pack_size { get; set; }
        public string net_selling_price { get; set; }
        public string offer_accepted { get; set; }

    }

    public class RootOfferAccepted
    {
        public string transaction_number { get; set; }
        public string transaction_date { get; set; }

        public string retailer_present { get; set; }

        public string stockist_present { get; set; }

        public List<OfferAccepted> offers_accepted { get; set; }
    }

    public class RootObjectOfferAccepted
    {
        public int count { get; set; }
        public RootOfferAccepted root { get; set; }
    }
}