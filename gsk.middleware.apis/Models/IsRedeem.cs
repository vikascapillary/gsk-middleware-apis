﻿namespace gsk.middleware.apis.Models
{
    public class IsRedeem
    {
    }
    public class IsRedeemStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class IsRedeemItemStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class IsRedeemRedeemable
    {
        public string mobile { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public int points { get; set; }
        public string is_redeemable { get; set; }
        public int points_redeem_value { get; set; }
        public int points_redeem_local_value { get; set; }
        public string points_currency_ratio { get; set; }
        public IsRedeemItemStatus item_status { get; set; }
    }

    public class IsRedeemPoints
    {
        public IsRedeemRedeemable redeemable { get; set; }
    }

    public class IsRedeemResponse
    {
        public IsRedeemStatus status { get; set; }
        public IsRedeemPoints points { get; set; }
    }

    public class IsRedeemRootObject
    {
        public IsRedeemResponse response { get; set; }
    }
}