﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models.ResponseApp
{
    public class ResponseToApp
    {
    }

    public class Field
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class CustomField
    {
        public List<Field> field { get; set; }
    }

    public class Field2
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class ExtendedField
    {
        public List<Field2> field { get; set; }
    }

    public class PromotionPointsStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public string total { get; set; }
        public string success_count { get; set; }
    }

    public class RedeemedStore
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RedeemedTill
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RedeemPoint
    {
        public int id { get; set; }
        public int program_id { get; set; }
        public int points_redeemed { get; set; }
        public string transaction_number { get; set; }
        public string validation_code { get; set; }
        public string redeemed_time { get; set; }
        public string redeemed_at { get; set; }
        public string notes { get; set; }
        public RedeemedStore redeemed_store { get; set; }
        public RedeemedTill redeemed_till { get; set; }
    }

    public class RedemptionsStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class ExpiredPoints
    {
        public List<Item> item { get; set; }
    }

    public class Customer2
    {
        public List<Item2> item { get; set; }
    }

    public class Item2
    {
        public int points { get; set; }
        public string expiry_date { get; set; }
        public IssuedAt issued_at { get; set; }
        public string issued_on { get; set; }
        public string program_id { get; set; }
        public PromotionSource promotion_source { get; set; }
    }

    public class IssuedAt
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class PromotionSource
    {
        public string source_type { get; set; }
        public string source_id { get; set; }
    }


    public class Item
    {
        public int points { get; set; }
        public string expired_on { get; set; }
        public string program_id { get; set; }
    }

    public class Lineitems
    {
        public List<object> item { get; set; }
    }

    public class PromotionPoints
    {
        public Customer2 customer { get; set; }
        public Transactions transactions { get; set; }
        public Lineitems lineitems { get; set; }
    }

    public class RootObject
    {
        public string mobile { get; set; } // done
        public List<string> lastname { get; set; }// done
        public List<string> firstname { get; set; } // done
        public List<CustomField> custom_fields { get; set; }//done
        public List<List<ExpiredPoints>> expired_points { get; set; }
        public List<ExtendedField> extended_fields { get; set; }//done
        public List<List<Item2>> promotion_points { get; set; }
        public PromotionPointsStatus promotion_points_status { get; set; }//done
        public List<RedeemPoint> redeem_point { get; set; }//done
        public RedemptionsStatus redemptions_status { get; set; } // done
        public string external_id { get; set; }// done
        public PromotionPointsStatus status { get; set; } // done
    }

    public class Root
    {
        public RootObject root { get; set; }// done
    }
}