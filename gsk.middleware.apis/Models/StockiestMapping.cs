﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models
{
    public class StockiestMapping
    {
        public string name { get; set; }
        public int pin_code { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string gst_no { get; set; }
        public string stockiest_code { get; set; }
    }

    public class RootStockiestMapping
    {
        public Status status { get; set; }
        public List<StockiestMapping> stockiest_mapping { get; set; }

    }
}