﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class SkuMappingFailure
    {
        public string stockiest_code { get; set; }
        public string invoice_number { get; set; }
        public string stockiest_sku_desc { get; set; }
        public string invoice_date { get; set; }
        public string failure_date { get; set; }
    }

    public class RootSkuMappingFailure
    {
        public List<SkuMappingFailure> sku_mapping_failure { get; set; }
    }

    public class RootObjectSkuMappingFailure
    {
        public RootSkuMappingFailure root { get; set; }
    }
}