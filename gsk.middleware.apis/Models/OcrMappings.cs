﻿using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Data;


namespace gsk.middleware.apis.Models
{
    public class OcrMappings
    {
    }

    public class MFG
    {
        private Logger logger;
        public static MFG GetInstance { get; } = new MFG();
        private MFG()
        {
            logger = LogManager.GetCurrentClassLogger();
        }
    }
    public class ProductMapping
    {
        private Logger logger;
        public static ProductMapping GetInstance { get; } = new ProductMapping();
        private ProductMapping()
        {
            logger = LogManager.GetCurrentClassLogger();
        }
    }
    public class ProductMaster
    {
        private Logger logger;
        public static ProductMaster GetInstance { get; } = new ProductMaster();
        private ProductMaster()
        {
            logger = LogManager.GetCurrentClassLogger();
        }
    }

    public class FailurePrudct
    {
        public string manufacturer { get; set; }
        public string product { get; set; }
        public string quantity { get; set; }
        public string item_code { get; set; }
    }
}