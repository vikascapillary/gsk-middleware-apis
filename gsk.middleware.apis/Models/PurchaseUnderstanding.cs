﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class PurchaseUnderstanding
    {
        public string id { get; set; }
        public string product_id { get; set; }
        public string sku_name { get; set; }
        public string pharmacy_code { get; set; }
        public string pharmacy_name { get; set; }
        public string brick_name { get; set; }
        public string brand_name { get; set; }
        public string rep_code { get; set; }
        public string rep_name { get; set; }
        public string offer_plan { get; set; }
        public string offer_status { get; set; }
        public string accepted_plan { get; set; }
        public string frequency { get; set; }
        public string offer_status_date { get; set; }
        public string offer_expiry_date { get; set; }
        public string current_dt { get; set; }
        public string reason_for_rejection { get; set; }
        public string opportunity_value { get; set; }
        public string commited_value { get; set; }
        public string total_cost { get; set; }
        public string total_margin { get; set; }
        public string total_margin_baseline { get; set; }
        public string total_discount { get; set; }
        public string total_discount_baseline { get; set; }
        public string secured_roi { get; set; }
        public string commited_volume { get; set; }
        public string commited_discount { get; set; }
        public string months_committed { get; set; }
        public string days_commited { get; set; }
        public string days_done { get; set; }
        public string days_remaining { get; set; }
        public string target_achieved { get; set; }
        public string last_refreshed { get; set; }
    }

    public class RootPurchaseUnderstanding
    {
        public Status status { get; set; }
        public List<PurchaseUnderstanding> purchase_understandings { get; set; }
    }
    public class RootObjectPurchaseUnderstanding
    {
        public RootPurchaseUnderstanding root { get; set; }
    }
}