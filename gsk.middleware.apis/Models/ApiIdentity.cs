﻿using System;
using System.Security.Principal;

namespace gsk.middleware.apis.Models
{
    public class ApiIdentity : IIdentity
    {
        public User User { get; private set; }

        public ApiIdentity(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            User = user;
        }

        public string Name => User.UserName;

        public string AuthenticationType => "Basic";

        public bool IsAuthenticated => true;
    }
}