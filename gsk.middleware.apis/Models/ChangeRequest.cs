﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models.ChangeRequest
{
    public class Customer
    {
        public string mobile { get; set; }
    }

    public class Request
    {
        public string reference_id { get; set; }
        public string type { get; set; }
        public string base_type { get; set; }
        public Customer customer { get; set; }
        public string old_value { get; set; }
        public string new_value { get; set; }
        public string client_auto_approve { get; set; }
    }

    public class Root
    {
        public Request request { get; set; }
    }

    public class RootObject
    {
        public Root root { get; set; }
    }
}