﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace gsk.middleware.apis.Models.Ocr.Data
{
    public class OcrImageInvoice
    {
        public string image_id { get; set; }
        public string ocr_job_id { get; set; }
        public string job_id_status { get; set; }
        public string image_update_time { get; set; }
        public OcrData ocrData { get; set; }
    }
    public class Root
    {
        public List<OcrImageInvoice> ocr_image_invoice { get; set; }
    }

    public class RootObject
    {
        public Root root { get; set; }
    }

    public class OcrData
    {
        public List<Table> table { get; set; }

        [JsonProperty(PropertyName = "Bill info")]
        public List<BillInfo> Bill_info { get; set; }
        public RuntimeInfo Runtime_info { get; set; }
    }

    public class Table
    {
        public string Sno { get; set; }
        public string file_name { get; set; }

        private string mfgName = string.Empty;
        public string manufacturer
        {
            get { return mfgName; }
            set{mfgName =( string.IsNullOrEmpty(value) ? "" : value).Trim();}
        }

        private string productName = string.Empty;
        public string product
        {
            get { return productName; }
            set { productName = (string.IsNullOrEmpty(value) ? "" : value).Trim(); }
        }

        public string quantity { get; set; }
        public string rate { get; set; }
        public string amount { get; set; }
        public string invoiceNo { get; set; }
        public string InvoiceDate { get; set; }
    }

    public class BillInfo
    {
        public string stockist_present { get; set; }
        public string retailer_present { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
    }

    public class Blurness
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class Inferencing
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class HoughLines
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class GetIntersectionPoint
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class BestFourPoints
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class FourBlockFilter
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class ReceiptDetection
    {
        public HoughLines hough_lines { get; set; }
        public GetIntersectionPoint get_intersection_point { get; set; }
        public BestFourPoints best_four_points { get; set; }
        public FourBlockFilter four_block_filter { get; set; }
    }

    public class OrientationCorrection
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class PreProcessing
    {
        public Blurness blurness { get; set; }
        public Inferencing inferencing { get; set; }
        public ReceiptDetection receipt_detection { get; set; }
        public OrientationCorrection orientation_correction { get; set; }
    }

    public class GoogleVisionApiCall
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class HeaderDetection
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class InvoiceDate
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class InvoiceNumber
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class ProductColumnDetection
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class ProductColumnJoining
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class RateAmountColumnDetection
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class ManufacturingColumnDetection
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class TableExtraction
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class PostProcessing
    {
        public GoogleVisionApiCall google_vision_api_call { get; set; }
        public HeaderDetection header_detection { get; set; }
        public InvoiceDate invoice_date { get; set; }
        public InvoiceNumber invoice_number { get; set; }
        public ProductColumnDetection product_column_detection { get; set; }
        public ProductColumnJoining product_column_joining { get; set; }
        public RateAmountColumnDetection rate_amount_column_detection { get; set; }
        public ManufacturingColumnDetection manufacturing_column_detection { get; set; }
        public TableExtraction table_extraction { get; set; }
    }

    public class RuntimeInfo
    {
        public PreProcessing pre_processing { get; set; }
        public PostProcessing post_processing { get; set; }
    }

    public class HarmonizatedDate
    {
        public string product { get; set; }
        public string quantity { get; set; }
        public string mfg_name { get; set; }
        public string item_code { get; set; }
        public string item_desc { get; set; }
    }
}