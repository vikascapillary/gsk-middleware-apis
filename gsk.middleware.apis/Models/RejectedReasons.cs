﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class RejectedReasons
    {
        public string reason_description { get; set; }
    }

    public class RootRejectedReasons
    {
        public List<RejectedReasons> reason_descriptions { get; set; }
    }

    public class RootObjectRejectedReasons
    {
        public int count { get; set; }
        public RootRejectedReasons root { get; set; }
    }
}