﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models.ChangeResponse
{
    public class Status
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class Customer
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string external_id { get; set; }
        public int id { get; set; }
    }

    public class ItemStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class Request
    {
        public string reference_id { get; set; }
        public string id { get; set; }
        public string status { get; set; }
        public string requested_on { get; set; }
        public string type { get; set; }
        public string base_type { get; set; }
        public Customer customer { get; set; }
        public string old_value { get; set; }
        public string new_value { get; set; }
        public string old_type { get; set; }
        public string new_type { get; set; }
        public object misc_info { get; set; }
        public string transaction_id { get; set; }
        public string reason { get; set; }
        public string comments { get; set; }
        public ItemStatus item_status { get; set; }
    }

    public class Requests
    {
        public List<Request> request { get; set; }
    }

    public class Response
    {
        public Status status { get; set; }
        public Requests requests { get; set; }
    }

    public class RootObject
    {
        public Response response { get; set; }
    }
}