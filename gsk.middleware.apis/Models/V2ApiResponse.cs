﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models.V2ApiResponse
{
    public class RootObject
    {
        public int createdId { get; set; }
        public List<object> warnings { get; set; }
    }
}