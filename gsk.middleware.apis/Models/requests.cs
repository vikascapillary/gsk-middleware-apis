﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class requests
    {
        public RootObject rootObjects { get; set; }
    }
    public class Customer
    {
        public string mobile { get; set; }
        public string retailer_dl { get; set; }
        public string retailer_gst { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string retailer_code { get; set; }
    }

    public class ExtendedFields
    {
        public List<Field> field { get; set; }
    }

    public class Payment
    {
        public object mode { get; set; }
        public object value { get; set; }
    }

    public class Field
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class PaymentDetails
    {
        public List<Payment> payment { get; set; }
    }

    public class LineItem
    {
        public string serial { get; set; }
        public string amount { get; set; }
        public string description { get; set; }
        public string item_code { get; set; }
        public string qty { get; set; }
        public string rate { get; set; }
        public string value { get; set; }
        public string discount { get; set; }
        public string packsize { get; set; }
        public string type { get; set; }
        public string return_type { get; set; }
        public string transaction_number { get; set; }
        public string transaction_date { get; set; }
        public ExtendedFields extended_fields { get; set; }
    }

    public class LineItems
    {
        public List<LineItem> line_item { get; set; }
    }
    public class Transaction
    {
        public string type { get; set; }
        public string return_type { get; set; }
        public string store_til { get; set; }
        public string stockiest_code { get; set; }
        public string number { get; set; }
        public string amount { get; set; }
        public string notes { get; set; }
        public string billing_time { get; set; }
        public string gross_amount { get; set; }
        public string discount { get; set; }
        public Customer customer { get; set; }
        public string entered_by { get; set; }
        public ExtendedFields custom_fields { get; set; }
        public PaymentDetails payment_details { get; set; }
        public LineItems line_items { get; set; }
    }
    public class Root
    {
        public List<Transaction> transaction { get; set; }
    }

    public class RootObject
    {
        public Root root { get; set; }
    }
    public class Customers
    {
        public List<Customer> customer { get; set; }
    }
    public class ResponseCustomer
    {
        public Status status { get; set; }
        public Customers customers { get; set; }
    }
    public class RootObjectCustomer
    {
        public ResponseCustomer response { get; set; }
    }
}