﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class TransResponse
    {
    }

    public class TransStatus
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class TransPoints
    {
        public string issued { get; set; }
        public string redeemed { get; set; }
        public string returned { get; set; }
        public string expired { get; set; }
        public string returnedPointsOnBill { get; set; }
        public string expiry_date { get; set; }
        public string program_id { get; set; }
    }

    public class TransField
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class TransCustomFields
    {
        public List<Field> field { get; set; }
    }

    public class TransExtendedFields
    {
        public List<object> field { get; set; }
    }

    public class TransLineItems
    {
        public List<LineItem> line_item { get; set; }
    }

    public class TransTransaction
    {
        public string id { get; set; }
        public string number { get; set; }
        public string type { get; set; }
        public string return_type { get; set; }
        public string amount { get; set; }
        public string outlier_status { get; set; }
        public string delivery_status { get; set; }
        public string notes { get; set; }
        public string billing_time { get; set; }
        public string auto_update_time { get; set; }
        public string gross_amount { get; set; }
        public string discount { get; set; }
        public string store { get; set; }
        public string store_code { get; set; }
        public string returned_points_on_bill { get; set; }
        public TransCustomFields custom_fields { get; set; }
        public TransExtendedFields extended_fields { get; set; }
        public List<object> coupons { get; set; }
        public int basket_size { get; set; }
        public TransLineItems line_items { get; set; }
    }

    public class TransTransactions
    {
        public List<TransTransaction> transaction { get; set; }
    }

    public class TransWarnings
    {
        public List<object> warning { get; set; }
    }

    public class TransItemStatus
    {
        public string success { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }

    public class TransCustomer
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string user_id { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public int lifetime_points { get; set; }
        public int lifetime_purchases { get; set; }
        public int loyalty_points { get; set; }
        public string current_slab { get; set; }
        public string registered_on { get; set; }
        public string updated_on { get; set; }
        public string type { get; set; }
        public string source { get; set; }
        public int count { get; set; }
        public string start { get; set; }
        public string delayed_points { get; set; }
        public string delayed_returned_points { get; set; }
        public string total_available_points { get; set; }
        public string total_returned_points { get; set; }
        public string rows { get; set; }
        public TransTransactions transactions { get; set; }
        public TransItemStatus item_status { get; set; }
    }

    public class TransResp
    {
        public TransStatus status { get; set; }
        public TransCustomer customer { get; set; }

        public TransTransactions transactions { get; set; }

    }

    public class TransRootObject
    {
        public TransResp response { get; set; }
    }

    public class ImageHistroy
    {
        public string invoice_number { get; set; }
        public string image_id { get; set; }
        public string updated_by { get; set; }
        public string updated_at { get; set; }
        public string status { get; set; }
    }
    public class RootResponse
    {
        public Status status { get; set; }
        public string remark { get; set; }
        public ImageInvoice image_invoice { get; set; }
        public RooObjectGetS3URL s3_response { get; set; }
        public List<ImageHistroy> imageHistroys { get; set;}
        public List<TransTransaction> transaction { get; set; }   

    }
}