﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class IdentifierItemDesc
    {
        public string id { get; set; }
        public string gsk_code { get; set; }
        public string gsk_desc { get; set; }
        public string gsk_pack_size { get; set; }
        public string stockiest_item_desc { get; set; }
        public string price { get; set; }
    }

    public class IntouchPrice
    {
        public string item_code { get; set; }
        public string price { get; set; }
        public string desc { get; set; }
        public string pack_size { get; set; }
    }

    public class RootIntouchPrice
    {
        public List<IntouchPrice> intouch_price { get; set; }
    }

    public class RootIdentifierItemDesc
    {
        public List<IdentifierItemDesc> gsk_identifiers { get; set; }
    }
    public class RootObjectIdentifierItemDesc
    {
        public RootIdentifierItemDesc root { get; set; }
    }

}