﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class responses
    {
    }
    public class Status
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class Attribute
    {
        public string name { get; set; }
        public string value { get; set; }
    }


    public class Attributes
    {
        public List<Attribute> attribute { get; set; }
    }

    public class Item
    {
        public string item_id { get; set; }
        public string sku { get; set; }
        public string item_ean { get; set; }
        public string price { get; set; }
        public string img_url { get; set; }
        public string in_stock { get; set; }
        public string description { get; set; }
        public object long_description { get; set; }
        public object size { get; set; }
        public object style { get; set; }
        public object brand { get; set; }
        public object color { get; set; }
        public object category { get; set; }
        public Attributes attributes { get; set; }
        public string added_on { get; set; }
        public ItemStatus item_status { get; set; }
    }

    public class Product
    {
        public List<Item> item { get; set; }
    }

    public class ItemStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class TransactionResponse
    {
        public string number { get; set; }
        public string record_id { get; set; }
        public string billing_time { get; set; }
        public string stockiestCode { get; set; }
        public string type { get; set; }
        public string delivery_status { get; set; }
        public ItemStatus item_status { get; set; }
        public string entered_by { get; set; }
        public Customer customer { get; set; }
    }

    public class Transactions
    {
        public List<TransactionResponse> transaction { get; set; }
    }

    public class Response
    {
        public Status status { get; set; }
        public Product product { get; set; }
        public Transactions transactions { get; set; }
    }

    public class RootObjectAddResponse
    {
        public Response response { get; set; }
    }

}