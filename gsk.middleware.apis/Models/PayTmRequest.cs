﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class PayTmRequest
    {
        public string requestType { get; set; }
        public string merchantGuid { get; set; }
        public string salesWalletName { get; set; }
        public string salesWalletGuid { get; set; }
        public string payeeEmailId { get; set; }
        public string payeeSsoId { get; set; }
        public string merchantOrderId { get; set; }
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public string callbackURL { get; set; }
        public string payeePhoneNumber { get; set; }
        public string appliedToNewUsers { get; set; }
        public string pendingDaysLimit { get; set; }

    }

    public class PayTmReqestFromApp
    {
        public string amount { get; set; }
        public string pay_tm_cap_mobile { get; set; }
    }

    public class PayTmResponse
    {
        public object type { get; set; }
        public object requestGuid { get; set; }
        public string orderId { get; set; }
        public object status { get; set; }
        public string statusCode { get; set; }
        public string statusMessage { get; set; }
        public object response { get; set; }
        public object metadata { get; set; }
    }

    public class RootObejectPayTM
    {
        public PayTmRequest request { get; set; }
        public string metadata { get; set; }
        public string ipAddress { get; set; }
        public string platformName { get; set; }
        public string operationType { get; set; }
        public string points { get; set; }
    }

    public class RootObjectPayTmFromApp
    {
        public PayTmReqestFromApp request { get; set; }
        public string metadata { get; set; }
        public string ipAddress { get; set; }
        public string points { get; set; }
    }

    public class PayTMStatusCheck
    {
        public string requestType { get; set; }
        public string txnType { get; set; }
        public string txnId { get; set; }
        public string mId { get; set; }
    }

    public class RootObjectPayTmStatusCheck
    {
        public PayTMStatusCheck request { get; set; }
        public string operationType { get; set; }
        public string ipAddress { get; set; }
        public string platformName { get; set; }

    }

    public class TxnList
    {
        public string txnGuid { get; set; }
        public double txnAmount { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public object txnErrorCode { get; set; }
        public string ssoId { get; set; }
        public string txnType { get; set; }
        public string merchantOrderId { get; set; }
        public string pgTxnId { get; set; }
        public string pgRefundId { get; set; }
        public object cashbackTxnId { get; set; }
        public bool isLimitPending { get; set; }
        public string mId { get; set; }
    }

    public class OrderIds
    {
        public string order_id { get; set; }
    }

    public class RootOrderIds
    {
        public Status status { get; set; }
        public List<OrderIds> order_ids { get; set; }
    }

    public class StatusResponseRootObejct
    {
        public Status status { get; set; }
        public PayTmResponse paytm_amount_add_response { get; set; }
        public PayTmResponse paytm_trans_status_failure { get; set; }
        public Status txn_status_check { get; set; }
        public string order_id { get; set; }
        public List<TxnList> txnList { get; set; }
    }
}