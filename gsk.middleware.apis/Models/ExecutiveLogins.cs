﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class ExecutiveLogins
    {
        public string name { get; set; }
        public string empId { get; set; }
        public string loginId { get; set; }
        public string newLogin { get; set; }
        public string password { get; set; }
        public string createdDate { get; set; }
        public string lastModifiedDate { get; set; }
    }

    public class RootExecutiveLogins
    {
        public List<ExecutiveLogins> logins { get; set; }
    }
    public class RootObjectExecutiveLogins
    {
        public RootExecutiveLogins root { get; set; }
    }
}