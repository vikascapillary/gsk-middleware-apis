﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class RetailerMapping
    {
    }
    public class IdentifierRetailer
    {
        public string id { get; set; }
        public string gsk_retailer_code { get; set; }
        public string gsk_retailer_name { get; set; }
        public string stockiest_retailer_identifier { get; set; }
        public string stockiest_retailer_identifier_type { get; set; }
    }

    public class RootIdentifierRetailer
    {
        public List<IdentifierRetailer> retailers_mapping { get; set; }
    }
    public class RootObjectIdentifierRetailer
    {
        public RootIdentifierRetailer root { get; set; }
    }

}