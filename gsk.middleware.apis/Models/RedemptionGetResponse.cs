﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models.RedemptionResponse
{
    public class RedemptionGetResponse
    {
    }

    public class Status
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class Coupons
    {
        public object coupon { get; set; }
    }

    public class RedeemedStore
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RedeemedTill
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class Point
    {
        public int id { get; set; }
        public int program_id { get; set; }
        public int points_redeemed { get; set; }
        public string transaction_number { get; set; }
        public string validation_code { get; set; }
        public string redeemed_time { get; set; }
        public string redeemed_at { get; set; }
        public string notes { get; set; }
        public RedeemedStore redeemed_store { get; set; }
        public RedeemedTill redeemed_till { get; set; }
    }

    public class Points
    {
        public List<Point> point { get; set; }
    }

    public class Redemptions
    {
        public Coupons coupons { get; set; }
        public Points points { get; set; }
    }

    public class Customer
    {
        public string mobile { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int rows { get; set; }
        public object coupons_count { get; set; }
        public int points_count { get; set; }
        public string coupons_start_id { get; set; }
        public int points_start_id { get; set; }
        public Redemptions redemptions { get; set; }
    }

    public class Response
    {
        public Status status { get; set; }
        public Customer customer { get; set; }
    }

    public class RootObject
    {
        public Response response { get; set; }
    }
}