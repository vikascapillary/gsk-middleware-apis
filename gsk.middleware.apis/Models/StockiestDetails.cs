﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class StockiestDetails
    {

    }
    public class Stockiest
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string integration { get; set; }
    }

    public class RootStockiest
    {
        public List<Stockiest> gsk_identifiers { get; set; }
    }
    public class RootObjectStockiest
    {
        public RootStockiest root { get; set; }
    }
}