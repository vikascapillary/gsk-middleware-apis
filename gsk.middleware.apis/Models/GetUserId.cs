﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models.GetUserId
{
    public class Status
    {
        public string success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
        public string total { get; set; }
        public string success_count { get; set; }
    }

    public class RegisteredStore
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RegisteredTill
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class FraudDetails
    {
        public string status { get; set; }
        public string marked_by { get; set; }
        public string modified_on { get; set; }
    }

    public class Field
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class CustomFields
    {
        public List<Field> field { get; set; }
    }

    public class Field2
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class ExtendedFields
    {
        public List<Field2> field { get; set; }
    }

    public class Transaction
    {
        public string id { get; set; }
        public string number { get; set; }
        public string type { get; set; }
        public string created_date { get; set; }
        public string store { get; set; }
    }

    public class Transactions
    {
        public List<Transaction> transaction { get; set; }
    }

    public class Coupons
    {
        public List<object> coupon { get; set; }
    }

    public class Warnings
    {
        public List<object> warning { get; set; }
    }

    public class ItemStatus
    {
        public string success { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public Warnings warnings { get; set; }
    }

    public class Customer
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public int lifetime_points { get; set; }
        public int lifetime_purchases { get; set; }
        public int loyalty_points { get; set; }
        public string current_slab { get; set; }
        public string registered_on { get; set; }
        public string updated_on { get; set; }
        public string type { get; set; }
        public string source { get; set; }
        public List<object> identifiers { get; set; }
        public object gender { get; set; }
        public string registered_by { get; set; }
        public RegisteredStore registered_store { get; set; }
        public RegisteredTill registered_till { get; set; }
        public FraudDetails fraud_details { get; set; }
        public string trackers { get; set; }
        public string user_id { get; set; }
        public object current_nps_status { get; set; }
        public CustomFields custom_fields { get; set; }
        public ExtendedFields extended_fields { get; set; }
        public Transactions transactions { get; set; }
        public Coupons coupons { get; set; }
        public List<object> notes { get; set; }
        public ItemStatus item_status { get; set; }
    }

    public class Customers
    {
        public List<Customer> customer { get; set; }
    }

    public class Response
    {
        public Status status { get; set; }
        public Customers customers { get; set; }
    }

    public class RootObject
    {
        public Response response { get; set; }
    }
}