﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class ImageInvoice
    {
        public string id { get; set; }
        public string retailar_code { get; set; }
        public string ocr_job_id { get; set; }
        public string stockiest_code { get; set; }
        public string dealer_name { get; set; }
        public string drug_license { get; set; }
        public string gst { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string pin { get; set; }
        public string image_id { get; set; }
        public string image_status { get; set; }
        public string image_upload_time { get; set; }
        public string image_update_time { get; set; }
        public string updated_by { get; set; }
        public string storeName { get; set; }
        public string storeDescription { get; set; }
        public string storeId { get; set; }
        public string transaction_number { get; set; }
    }

    public class RetailerUploadedImage
    {
        public string retailar_code { get; set; }
    }

    public class RootRetailerUploadedImage
    {
        public List<RetailerUploadedImage> image_invoice { get; set; }
    }

    public class RootObjectRetailerUploadedImage
    {
        public RootRetailerUploadedImage root { get; set; }
        public int count { get; set; }
    }
    public class RootImageInvoice
    {
        public Status status { get; set; }
        public List<ImageInvoice> image_invoice { get; set; }

    }
    public class RootObjectImageInvoice
    {
        public int count { get; set; }
        public int page_count { get; set; }
        public RootImageInvoice root { get; set; }
    }

    public class AddResponse
    {
        public Status status { get; set; }
    }

    public class RootAddResponse
    {
        public AddResponse response { get; set; }
    }

    public class RooObjectGetS3URL
    {
        public string data { get; set; }
        public Status status { get; set; }
    }

}