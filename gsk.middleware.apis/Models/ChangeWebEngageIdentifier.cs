﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace gsk.middleware.apis.Models.ChangeWebEngageIdentifier
{
    public class Add
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class Remove
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class RootObject
    {
        public List<Add> add { get; set; }
        public List<Remove> remove { get; set; }
    }
}