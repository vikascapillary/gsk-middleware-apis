﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace gsk.middleware.apis.Models
{
    public sealed class ImageAssigningToExectutive
    {
        private Logger logger;
        private readonly string connectionString;
        private GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        public static Queue<string> executive = new Queue<string>();
        private static ImageAssigningToExectutive instance = null;
        public static ImageAssigningToExectutive GetInstance
        {
            get
            {
                if (executive.Count > 0)
                {
                    return instance;
                }
                else
                {
                    instance = null;
                }

                return new ImageAssigningToExectutive();
            }
        }
        private ImageAssigningToExectutive()
        {
            logger = LogManager.GetCurrentClassLogger();
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
          connectionString = string.Format("{0}&e;", connectionString);
            try
            {
                string query = ConfigurationManager.AppSettings["ExecutiveLogin"]+@" where `exec_login`=1";
                if (executive.Count <= 0)
                {
                    System.Data.DataTable dt = gi.GetDataMapping(query, connectionString);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[i]["login_id"].ToString()))
                            {
                                executive.Enqueue(dt.Rows[i]["login_id"].ToString());
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }
    }
}