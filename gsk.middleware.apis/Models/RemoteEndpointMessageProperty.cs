﻿namespace gsk.middleware.apis.Models
{
    internal class RemoteEndpointMessageProperty
    {
        public static string Name { get; internal set; }
        public string Address { get; internal set; }
    }
}