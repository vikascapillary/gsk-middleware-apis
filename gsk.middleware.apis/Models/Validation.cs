﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace gsk.middleware.apis.Models
{
    public class Validation
    {
        public static double AmountCheck(string inputValue)
        {
            double returnValue = 0.0;
            bool check = double.TryParse(inputValue, out double value);
            if (check)
            {
                returnValue = value;
            }
            else
            {
                returnValue = -1;
            }

            return returnValue;

        }

        public static double IntCheck(string inputValue)
        {
            int returnValue = 0;

            bool check = int.TryParse(inputValue, out int value);
            if (check)
            {
                returnValue = value;
            }
            else
            {
                returnValue = -1;
            }
            return returnValue;
        }

        public static DateTime IST()
        {
            DateTime timeUtc = DateTime.UtcNow;
            TimeZoneInfo istZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime istTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, istZone);
            return istTime;
        }

        public static bool IsMD5(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            return Regex.IsMatch(input, "^[0-9a-fA-F]{32}$", System.Text.RegularExpressions.RegexOptions.Compiled);
        }
    }
}