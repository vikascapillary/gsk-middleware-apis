﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebGrease;
using log4net;
using log4net.Repository.Hierarchy;
using log4net.Layout;
using log4net.Appender;
using log4net.Core;

namespace gsk.middleware.apis
{
    public class CapillaryLogger
    {
        public static void Setup()
        {
            var hierarchy = (Hierarchy)log4net.LogManager.GetRepository();
            var logPath = AppDomain.CurrentDomain.BaseDirectory;
            var patternLayout = new PatternLayout
            {
                ConversionPattern = "%date [%thread] %-5level %logger - %message%newline"
            };
            patternLayout.ActivateOptions();
            var roller = new RollingFileAppender
            {
                AppendToFile = true,
                File = string.Format(@"{0}/CapillaryLogs/ServiceLog.txt", logPath),
                Layout = patternLayout,
                MaxSizeRollBackups = 20,
                MaximumFileSize = "100MB",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);
            var memory = new MemoryAppender();
            memory.ActivateOptions();
            hierarchy.Root.AddAppender(memory);
            hierarchy.Root.Level = Level.Debug;
            hierarchy.Configured = true;
        }
    }
}