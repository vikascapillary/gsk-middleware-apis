﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class TransactionAddRequest
    {
    }
    public class IntouchCustomer
    {
        public string external_id { get; set; }
    }

    public class IntouchLineItem
    {
        public string serial { get; set; }
        public string amount { get; set; }
        public string description { get; set; }
        public string item_code { get; set; }
        public string qty { get; set; }
        public string rate { get; set; }
        public string value { get; set; }
        public string discount { get; set; }
    }

    public class IntouchLineItems
    {
        public List<IntouchLineItem> line_item { get; set; }
    }

    public class IntouchTransaction
    {
        public string type { get; set; }
        public string number { get; set; }
        public string amount { get; set; }
        public string billing_time { get; set; }
        public string discount { get; set; }
        public IntouchCustomer customer { get; set; }
        public IntouchLineItems line_items { get; set; }
    }

    public class RootIntouch
    {
        public List<IntouchTransaction> transaction { get; set; }
    }

    public class RootObjectIntouch
    {
        public Root root { get; set; }
    }
}