﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class RedemptionGet
    {
    }

    public class RedemptionGetStatus
    {
        public bool success { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class RedemptionGetCoupons
    {
        public object coupon { get; set; }
    }

    public class RedemptionGetRedeemedStore
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RedemptionGetRedeemedTill
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RedemptionGetPoint
    {
        public int id { get; set; }
        public int program_id { get; set; }
        public int points_redeemed { get; set; }
        public string transaction_number { get; set; }
        public string validation_code { get; set; }
        public string redeemed_time { get; set; }
        public string redeemed_at { get; set; }
        public string notes { get; set; }
        public RedemptionGetRedeemedStore redeemed_store { get; set; }
        public RedemptionGetRedeemedTill redeemed_till { get; set; }
    }

    public class RedemptionGetPoints
    {
        public List<RedemptionGetPoint> point { get; set; }
    }

    public class RedemptionGetRedemptions
    {
        public RedemptionGetCoupons coupons { get; set; }
        public RedemptionGetPoints points { get; set; }
    }

    public class RedemptionGetCustomer
    {
        public string mobile { get; set; }
        public string email { get; set; }
        public string external_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int rows { get; set; }
        public object coupons_count { get; set; }
        public int points_count { get; set; }
        public string coupons_start_id { get; set; }
        public int points_start_id { get; set; }
        public RedemptionGetRedemptions redemptions { get; set; }
    }

    public class RedemptionGetResponse
    {
        public RedemptionGetStatus status { get; set; }
        public RedemptionGetCustomer customer { get; set; }
    }

    public class RedemptionGetRootObject
    {
        public RedemptionGetResponse response { get; set; }
    }
}