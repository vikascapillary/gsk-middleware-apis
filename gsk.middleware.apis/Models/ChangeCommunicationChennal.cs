﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models.ChangeCommunicationChennal
{
    public class Identifier
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class Meta
    {
        public string residence { get; set; }
    }

    public class CommChannel
    {
        public string type { get; set; }
        public string value { get; set; }
        public string primary { get; set; }
        public string verified { get; set; }
        public Meta meta { get; set; }
    }

    public class Profile
    {
        public List<Identifier> identifiers { get; set; }
        public List<CommChannel> commChannels { get; set; }
        public string source { get; set; }
        public string accountId { get; set; }
    }

    public class RootObject
    {
        public List<Profile> profiles { get; set; }
    }
}