﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gsk.middleware.apis.Models
{
    public class SmsSend
    {
    }

    public class Sm
    {
        public string to { get; set; }
        public string body { get; set; }
        public string scheduled_time { get; set; }
    }

    public class RootSms
    {
        public List<Sm> sms { get; set; }
    }

    public class RootObjectSms
    {
        public RootSms root { get; set; }
    }
}