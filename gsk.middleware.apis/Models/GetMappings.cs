﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

namespace gsk.middleware.apis.Models
{
    public class GetMappings
    {
        private MemoryCache cache = new MemoryCache("CacheKey");
        private MultiPageInvoiceHandling multiPageInvoiceHandling = MultiPageInvoiceHandling.GetInstance;
        private static readonly Logger logger;
        private static readonly string connectionString;
        private static GetGskIdentifiers gi = GetGskIdentifiers.GetInstance;
        public static DataTable mfg, pmapping, pmaster, rejectReason, stockiestMapping;
        static object mfgTable = new object();
        const string mfgCache = "mfgCache";
        static object productMappingTable = new object();
        static string productMappingCache = "productMappingCache";
        static object productMasterTable = new object();
        static string productMasterCache = "productMasterCache";
        static object rejectReasonTable = new object();
        static string rejectReasonCache = "rejectReasonCache";
        static object stockiestMappingTable = new object();
        static string stockiestMappingnCache = "stockiestMappingCache";

        static GetMappings()
        {
            logger = LogManager.GetCurrentClassLogger();
            connectionString = ConfigurationManager.AppSettings["MySqlConnectionString"];
          connectionString = string.Format("{0}&e;", connectionString);
        }
        private static DataTable GetGskManufacture()
        {
            var mfg = gi.GetDataMapping(ConfigurationManager.AppSettings["GSK_Items"], connectionString);
            return mfg;
        }
        private static DataTable GetProductMapping()
        {
            var pmapping = gi.GetDataMapping(ConfigurationManager.AppSettings["Productmapping"], connectionString);
            return pmapping;
        }
        private static DataTable GetProductMater()
        {
            var pmaster = gi.GetDataMapping("SELECT  `item_code` ,  `item_description` ,  `pack_size` ,  `net_selling_price`   FROM  `productmaster` WHERE  `offer_accepted` = 1", connectionString);
            return pmaster;
        }
        private static DataTable RejectReasons()
        {
            DataTable dt = gi.GetDataMapping("SELECT * FROM `rejected_reasons`", connectionString);
            return dt;
        }

        private static DataTable GetStockiestMaping()
        {
            DataTable dt = gi.GetDataMapping("SELECT * FROM `stockiest_mapping`", connectionString);
            return dt;
        }
        public static void Mappings()
        {
            
            mfg = MemoryCacheHelper.GetCachedData<DataTable>(mfgCache, mfgTable, 1, GetGskManufacture);
            pmapping = MemoryCacheHelper.GetCachedData<DataTable>(productMappingCache, productMappingTable, 600, GetProductMapping);
            pmaster = MemoryCacheHelper.GetCachedData<DataTable>(productMasterCache, productMasterTable, 600, GetProductMater);
            rejectReason = MemoryCacheHelper.GetCachedData<DataTable>(rejectReasonCache, rejectReasonTable, 600, RejectReasons);
            stockiestMapping = MemoryCacheHelper.GetCachedData<DataTable>(stockiestMappingnCache, stockiestMappingTable, 600, GetStockiestMaping);
        }
    }
}