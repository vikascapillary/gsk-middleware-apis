﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models.Ocr
{
    public class OcrProcessing
    {
    }
    public class OcrImageInvoice
    {
        public string image_id { get; set; }
        public string ocr_job_id { get; set; }
        public string job_id_status { get; set; }
        public string image_update_time { get; set; }
    }

    public class Root
    {
        public List<OcrImageInvoice> ocr_image_invoice { get; set; }
    }

    public class RootObject
    {
        public Root root { get; set; }
    }
}