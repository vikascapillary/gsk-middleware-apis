﻿using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Data;

namespace gsk.middleware.apis.Models
{
    public sealed class GetGskIdentifiers
    {
        private Logger logger;
        public static GetGskIdentifiers GetInstance { get; } = new GetGskIdentifiers();
        private GetGskIdentifiers()
        {
            logger = LogManager.GetCurrentClassLogger();
        }
        public DataTable GetDataMapping(string query, string conString)
        {
            DataTable dt = new DataTable();
            try
            {
                using (MySqlConnection connection = new MySqlConnection(string.Format("{0};", conString)))
                {
                    using (MySqlCommand cmd = new MySqlCommand(query))
                    {
                        cmd.Connection = connection;
                        connection.Open();
                        using (MySqlDataReader sdr = cmd.ExecuteReader())
                        {
                            dt = new DataTable("data");
                            dt.Load(sdr);
                        }
                        connection.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return dt;
        }

    }
}