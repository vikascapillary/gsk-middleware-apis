﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace gsk.middleware.apis.Models
{
    public class MultiPageInvoiceHandling
    {
        Logger logger;
        string customerTrasnactionGetApi, intouchTrasnactionAddApi;
        private ApiCall apiCall = ApiCall.GetInstance;
        public static MultiPageInvoiceHandling GetInstance { get; } = new MultiPageInvoiceHandling();
        private MultiPageInvoiceHandling()
        {
            logger = LogManager.GetCurrentClassLogger();
            customerTrasnactionGetApi = ConfigurationManager.AppSettings["IntouchTransactionGetAPI"];
            intouchTrasnactionAddApi = ConfigurationManager.AppSettings["IntouchTransactionAddAPI"];
        }

        public RootObjectAddResponse ValidateProduct(string request, string billnumber, string retailerCode, string iunid, string imageId)
        {
            RootObjectAddResponse ri = new RootObjectAddResponse();
            try
            {
                var counter = 1;
                Start:
                RootObject ro = new RootObject();
                LineItems unmatchedItems = new LineItems();
                LineItems requestItems = new LineItems();
                TransLineItems responseItems = new TransLineItems();
                TransRootObject resOthers = new TransRootObject();
                DataContractJsonSerializer custdes = new DataContractJsonSerializer(typeof(RootObject));
                using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(request)))
                {
                     ro = (RootObject)custdes.ReadObject(ms);
                }

                var customerTransactionGet = string.Format(@"{0}&external_id={1}&number={2}", customerTrasnactionGetApi, retailerCode,billnumber);
                string otherTransGetResponse = apiCall.IntouchMakeRequest(customerTransactionGet, "", iunid, "gsk123", "", "");
                DataContractJsonSerializer otherDeserializer = new DataContractJsonSerializer(typeof(TransRootObject));
                using (MemoryStream ms1 = new MemoryStream(Encoding.Unicode.GetBytes(otherTransGetResponse)))
                {
                    resOthers = (TransRootObject)otherDeserializer.ReadObject(ms1);
                }


                foreach (var transaction in resOthers.response.customer.transactions.transaction)
                {
                    responseItems = transaction.line_items;
                }

                foreach (var transaction in ro.root.transaction)
                {
                    var query = from t1 in transaction.line_items.line_item.AsEnumerable()
                                join t2 in responseItems.line_item.AsEnumerable()
                                on t1.item_code equals t2.item_code
                                into gj
                                from subnet in gj.DefaultIfEmpty()
                                select new ProductMatched
                                {
                                    request_item_code = t1.item_code,
                                    response_item_code = subnet == null ? string.Empty : subnet.item_code,
                                };

                    foreach (var p in query.ToList())
                    {
                        if (!string.IsNullOrEmpty(p.response_item_code))
                        {
                           transaction.line_items.line_item.RemoveAll(i => i.item_code == p.request_item_code);
                        }
                    }

                    if (transaction.line_items.line_item.Count() > 0)
                    {
                        if (billnumber.Contains("_"))
                        {
                            var arr = billnumber.Split('_');
                            counter = Convert.ToInt32(arr[1]);
                            counter++;
                            transaction.number = string.Format("{0}_{1}", arr[0], counter.ToString());
                        }
                        else
                        {
                            transaction.number = string.Format("{0}_{1}", billnumber, counter.ToString());
                        }
                        string json2 = JsonConvert.SerializeObject(ro, Formatting.Indented);
                        string d = apiCall.IntouchMakeRequest(intouchTrasnactionAddApi, json2, iunid, "gsk123", transaction.billing_time, imageId);
                        try
                        {
                            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(RootObjectAddResponse));
                            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(d)))
                            {
                                ri = (RootObjectAddResponse)deserializer.ReadObject(ms);
                                foreach (var trans in ri.response.transactions.transaction)
                                {
                                    if (trans.item_status.code == 604)
                                    {
                                        request = json2;
                                        billnumber = trans.number;
                                        goto Start;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.ToString());
                        }
                    }
                }

                #region Old Code
                //var query = from t1 in requestItems.line_item.AsEnumerable()
                //             join t2 in responseItems.line_item.AsEnumerable()
                //             on t1.item_code equals t2.item_code
                //             into gj
                //             from subnet in gj.DefaultIfEmpty()
                //             select new ProductMatched
                //             {
                //                 request_item_code = t1.item_code,
                //                 response_item_code = subnet == null ? string.Empty : subnet.item_code,
                //             };

                //foreach(var p in query.ToList())
                //{
                //    if (!string.IsNullOrEmpty(p.response_item_code))
                //    {
                //        var itemToRemove = requestItems.line_item.SingleOrDefault(i => i.item_code == p.request_item_code);
                //        if (itemToRemove!=null)
                //        {
                //            requestItems.line_item.Remove(itemToRemove);
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return ri;
        }
    }

    public class ProductMatched
    {
        public string request_item_code { get; set; }
        public string response_item_code { get; set; }
    }
}