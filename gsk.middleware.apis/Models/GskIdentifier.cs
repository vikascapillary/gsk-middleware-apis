﻿using System.Collections.Generic;

namespace gsk.middleware.apis.Models
{
    public class GskIdentifier
    {

    }
    public class Identifier
    {
        public string id { get; set; }
        public string gsk_identifier { get; set; }
    }

    public class RootIdentifier
    {
        public List<Identifier> gsk_identifiers { get; set; }
        public Status status { get; set; }
    }
    public class RootObjectIdentifier
    {
        public RootIdentifier root { get; set; }
    }
}